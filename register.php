<?php
	include("src/session_non_connected.php");
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("src/head_html.php"); ?>
		<link rel="stylesheet"  type="text/css" href="css/formulaire.css" />
	</head>
	<body>

		<div id="all">
			<?php include("src/header.php"); ?>
			
			<div id="content">
				<div class="card">
					<div class="card_title">
						<h1>Inscription</h1>
					</div>
					<div class="container">
						<div>
							<p class="error_msg" id="error_register">Veuillez indiquer vos données</p>
						</div>
						<form id="connexion" class="form_connexion" method="post">
							<fieldset>
								<div class="input_group">
									<input class="input_form" name="login" type="text" required>
									<span class="input_bar"></span>
									<label for="login">Login</label>
								</div>
								<div class="input_group">
									<input class="input_form" name="email" type="text" required>
									<span class="input_bar"></span>
									<label for="email">Email</label>
								</div>
								<div class="input_group">
									<input class="input_form" name="check_email" type="text" required>
									<span class="input_bar"></span>
									<label for="email">Confirmation email</label>
								</div>
								<div class="input_group">
									<input class="input_form" name="password" type="password" required>
									<span class="input_bar"></span>
									<label for="password">Mot de passe</label>
								</div>
								<div class="input_group">
									<input class="input_form" name="check_password" type="password" required>
									<span class="input_bar"></span>
									<label for="password">Confirmation mot de passe</label>
								</div>
								<div class="button_group">
									<button id="submit_form_connexion" class="button_form" name="register" type="submit">Inscription</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
				<div id="marge"></div>
			</div>

			<?php include("src/footer.php"); ?>
		</div>

		<script type="text/javascript" src="js/register.js"></script>

	</body>
</html>