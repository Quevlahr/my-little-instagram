function ajax(xmlhttp, form_data, changement, item)
{
	if (xmlhttp != null && form_data != null) {
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == XMLHttpRequest.DONE)
			{
				if (xmlhttp.status == 200)
				{
					if (xmlhttp.responseText['success'] !== null)
					{
						var response = JSON.parse(xmlhttp.responseText);
						document.getElementById('error_msg_id').innerHTML = response.message;
						if (response.success === true)
						{
							if (changement == "email" || changement == "login") {
								label = document.getElementById('change_'+changement+'_label');
								label.innerHTML = item;
								label.parentNode.firstChild.nextSibling.value = "";
							}
							else if (changement == "password") {
								inputs = document.querySelectorAll("#change_password input");
								inputs.forEach(function(input) {
									input.value = "";
								})
							}
						}
					}
				}
				else if (xmlhttp.status == 400)
				{
					console.log("There was an error 400");
				}
				else
				{
					console.log("Something else other than 200 was returned");
				}
			}
		};
		xmlhttp.open("POST", "src/check_change.php", true);
		xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlhttp.send(form_data);
	}
}

function change_notification()
{
	var form_notification = document.getElementById('change_notification');

	form_notification.addEventListener('submit', function(event) {
		var xmlhttp = new XMLHttpRequest(),
		notification = document.getElementById('change_notification_input').checked;

		var form_data = "id_user=" + id_user + "&notification=" + notification;

		event.preventDefault();

		ajax(xmlhttp, form_data, 'notification', notification);
	});
}

function change_login()
{
	var form_login = document.getElementById('change_login');

	form_login.addEventListener('submit', function(event) {
		var xmlhttp = new XMLHttpRequest(),
		login = document.getElementById("change_login_input").value;

		var form_data = "id_user=" + id_user + "&login=" + login;

		event.preventDefault();

		ajax(xmlhttp, form_data, 'login', login);
	});
}

function change_email()
{
	var form_email = document.getElementById('change_email');

	form_email.addEventListener('submit', function(event) {
		var xmlhttp = new XMLHttpRequest(),
		email = document.getElementById('change_email_input').value;

		var form_data = "id_user=" + id_user + "&email=" + email;

		event.preventDefault();

		ajax(xmlhttp, form_data, 'email', email);
	});
}

function change_password()
{
	var form_password = document.getElementById('change_password');

	form_password.addEventListener('submit', function(event) {
		var xmlhttp = new XMLHttpRequest(),
		old_password = document.getElementById('change_old_password_input').value,
		new_password = document.getElementById('change_new_password_input').value,
		check_password = document.getElementById('change_check_password_input').value;

		var form_data = "id_user=" + id_user + "&old_password=" + old_password + "&new_password=" + 
						new_password + "&check_password=" + check_password;

		event.preventDefault();

		ajax(xmlhttp, form_data, 'password', '');
	});
}

window.addEventListener("load", function(event) {
	change_login();
	change_email();
	change_password();
	change_notification();
});

