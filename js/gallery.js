var image = document.querySelector(".modal_image");
var modal = document.getElementById("modal_gallery");

function infiniteScroll()
{
	var body = document.body;
	var html = document.documentElement;

	var gallery = document.getElementById("unique_gallery");
	var offset = 20;
	var continue_infinite_scroll = true;

	window.addEventListener("load", function ()
	{
		var body_height = Math.max(body.scrollHeight, body.offsetHeight, 
							html.clientHeight, html.scrollHeight, html.offsetHeight);
		var window_height = window.innerHeight;
		if (window_height === body_height)
		{
			var xmlhttp = new XMLHttpRequest();
			var form_datas = "offset=" + offset + "&index=" + index;

			if (xmlhttp != null && form_datas != null)
			{
				xmlhttp.onreadystatechange = function()
				{
					if (xmlhttp.readyState === XMLHttpRequest.DONE)
					{
						if (xmlhttp.status === 200)
						{
							if (xmlhttp.responseText['success'] !== null)
							{
								var response = JSON.parse(xmlhttp.responseText);
								if (response['success'] === true)
								{
									for (var i = 0; i < response['nb_image']; i++)
									{
										var new_div = document.createElement('div');
										new_div.setAttribute("class", "item_gallery");
										var new_link = document.createElement('a');
										new_link.setAttribute("class", "link_image");
										new_link.setAttribute("href", "gallery.php?id_img="+response[i]['id_img']);
										var new_image = document.createElement('img');
										new_image.setAttribute("id", response[i]['id_img']);
										new_image.setAttribute("src", response[i]['path_img']);
										new_image.setAttribute("alt", response[i]['name_img']);
										new_link.appendChild(new_image);
										new_div.appendChild(new_link);
										gallery.appendChild(new_div);
										offset++;

										new_link.addEventListener("click", function(event)
										{
											event.preventDefault();
											create_modal_image(this);

										});
									}
								}
							}
						}
						else if (xmlhttp.status === 400)
						{
							console.log("There was an error 400");
						}
						else
						{
							console.log("Something else other than 200 was returned");
						}
					}
				};
				xmlhttp.open("POST", "src/infinite_scroll.php", true);
				xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				xmlhttp.send(form_datas);
			}
		}
	});

	window.addEventListener("scroll", function () 
	{
		var body_height = Math.max(body.scrollHeight, body.offsetHeight, 
							html.clientHeight, html.scrollHeight, html.offsetHeight);
		var window_height = window.innerHeight;
		if (window.scrollY === body_height - window_height && continue_infinite_scroll === true)
		{
			var xmlhttp = new XMLHttpRequest();
			var form_datas = "offset=" + offset + "&index=" + index;

			var loading = document.getElementById('load_infinite_scroll');
			loading.innerHTML = "Chargement...";

			if (xmlhttp != null && form_datas != null)
			{
				xmlhttp.onreadystatechange = function()
				{
					if (xmlhttp.readyState == XMLHttpRequest.DONE)
					{
						loading.innerHTML = "";
						if (xmlhttp.status === 200)
						{
							if (xmlhttp.responseText['success'] !== null)
							{
								var response = JSON.parse(xmlhttp.responseText);
								if (response['success'] === true)
								{
									if (response['nb_image'] == 0) {
										continue_infinite_scroll = false;
									}
									for (var i = 0; i < response['nb_image']; i++)
									{
										var new_div = document.createElement('div');
										new_div.setAttribute("class", "item_gallery");
										var new_link = document.createElement('a');
										new_link.setAttribute("class", "link_image");
										new_link.setAttribute("href", "gallery.php?id_img="+response[i]['id_img']);
										var new_image = document.createElement('img');
										new_image.setAttribute("id", response[i]['id_img']);
										new_image.setAttribute("src", response[i]['path_img']);
										new_image.setAttribute("alt", response[i]['name_img']);
										new_link.appendChild(new_image);
										new_div.appendChild(new_link);
										gallery.appendChild(new_div);
										offset++;

										new_link.addEventListener("click", function(event)
										{
											event.preventDefault();
											create_modal_image(this);

										});
									}
								}
							}
						}
						else if (xmlhttp.status === 400)
						{
							console.log("There was an error 400");
						}
						else
						{
							console.log("Something else other than 200 was returned");
						}
					}
				};
				xmlhttp.open("POST", "src/infinite_scroll.php", true);
				xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				xmlhttp.send(form_datas);
			}
		}
	});
};


function view_img()
{
    var close = document.querySelector(".close");
	var link_image = document.querySelectorAll(".link_image");

	for (var i = 0; i < link_image.length; i++) {
		link_image[i].addEventListener("click", function(event)
        {
            event.preventDefault();
            create_modal_image(this);
        });
	}

	close.addEventListener("click", function() {
		close_modal_image();
	});
}

function like_image() {

    var button_like = document.getElementById("button_like");
	button_like.addEventListener("click", function()
	{
		var xmlhttp = new XMLHttpRequest();
		var id_img = image.getAttribute("id").substr(6, image.getAttribute("id").length);
		var form_datas = "id_img=" + id_img;
		var nb_likes = document.getElementById("nb_likes");

		if (xmlhttp != null && form_datas != null) {
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState === XMLHttpRequest.DONE) {
					if (xmlhttp.status === 200) {
						if (xmlhttp.responseText['success'] !== null) {
							var response = JSON.parse(xmlhttp.responseText);
							if (response.success === true) {

								if (button_like.className === 'button_form_liked') {
									button_like.setAttribute('class', 'button_form_unliked');
								}
								else {
									button_like.setAttribute('class', 'button_form_liked');
								}

								var getNbLikes = new XMLHttpRequest();

								if (getNbLikes != null && form_datas != null) {
									getNbLikes.onreadystatechange = function() {
										if (getNbLikes.readyState === XMLHttpRequest.DONE) {
											if (getNbLikes.status === 200) {
												if (getNbLikes.responseText['success'] !== null) {
													var response = JSON.parse(getNbLikes.responseText);
													if (response['success'] === true) {
														nb_likes.innerHTML = parseInt(response['nb_likes']);
													}
												}
											}
											else if (getNbLikes.status === 400) {
												console.log("There was an error 400");
											}
											else {
												console.log("Something else other than 200 was returned");
											}
										}
									};
									getNbLikes.open("POST", "src/check_user_image.php", true);
									getNbLikes.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
									getNbLikes.send(form_datas);
								}


							}
						}

					}
					else if (xmlhttp.status === 400)
					{
						console.log("There was an error 400");
					}
					else
					{
						console.log("Something else other than 200 was returned");
					}
				}
			};
			xmlhttp.open("POST", "src/thumbs_up.php", true);
			xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xmlhttp.send(form_datas);
		}
	});
}


function delete_image() {

    var button_delete = document.getElementById("button_delete");
	button_delete.addEventListener("click", function()
	{
		var delete_confirmation = confirm("Êtes-vous sûr de vouloir supprimer cette photo ?");
		if (delete_confirmation === true)
		{
			var xmlhttp = new XMLHttpRequest();
			var id_img = image.getAttribute("id").substr(6, image.getAttribute("id").length);
			var form_datas = "id_img=" + id_img;

			if (xmlhttp !== null && form_datas !== null)
			{
				xmlhttp.onreadystatechange = function()
				{
					if (xmlhttp.readyState === XMLHttpRequest.DONE)
					{
						if (xmlhttp.status === 200)
						{
							if (xmlhttp.responseText['success'] !== null)
							{
								var response = JSON.parse(xmlhttp.responseText);
								if (response['success'] === true)
								{
									close_modal_image();
									var image_to_supp = document.getElementById(id_img);
									image_to_supp = image_to_supp.parentNode.parentNode;
									image_to_supp.parentNode.removeChild(image_to_supp);
								}
							}
						}
						else if (xmlhttp.status === 400)
						{
							console.log("There was an error 400");
						}
						else
						{
							console.log("Something else other than 200 was returned");
						}
					}
				};
				xmlhttp.open("POST", "src/delete_image.php", true);
				xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				xmlhttp.send(form_datas);
			}
		}
	});
}

function post_comment() {

    var comment_form = document.getElementById('comment_form');

	comment_form.addEventListener("submit", function(event)
	{
		event.preventDefault();

		var xmlhttp = new XMLHttpRequest();
		var id_img = image.getAttribute("id").substr(6, image.getAttribute("id").length);
		var comment = document.getElementById('textarea_comment');
		var length_comment = comment.value.length;

		if (length_comment === 0 && length_comment > 300) {
			return;
		}

		var form_datas = "id_img=" + id_img + "&comment=" + comment.value;

		if (xmlhttp != null && form_datas != null)
		{
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState === XMLHttpRequest.DONE)
				{
					if (xmlhttp.status === 200)
					{
						if (xmlhttp.responseText['success'] !== null)
						{
							var response = JSON.parse(xmlhttp.responseText);
							if (response.success === true) {
								display_comments();
								comment.value = '';
							}
						}
					}
					else if (xmlhttp.status === 400)
					{
						console.log("There was an error 400");
					}
					else
					{
						console.log("Something else other than 200 was returned");
					}
				}
			};
			xmlhttp.open("POST", "src/post_comment.php", true);
			xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xmlhttp.send(form_datas);
		}
	});
}

function display_comments() {

	var xmlhttp = new XMLHttpRequest();
	var id_img = image.getAttribute("id").substr(6, image.getAttribute("id").length);
	var form_datas = "id_img=" + id_img;
	var comment_display = document.getElementById('comment_display');

	if (xmlhttp != null && form_datas != null)
	{
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState === XMLHttpRequest.DONE)
			{
				if (xmlhttp.status === 200)
				{
					if (xmlhttp.responseText['success'] !== null)
					{
						var response = JSON.parse(xmlhttp.responseText);
						if (response.success === true) {
							while (comment_display.firstChild) {
								comment_display.removeChild(comment_display.firstChild);
							}
							for (var key in response.comments) {
								var new_div_display_login = document.createElement('div');
								new_div_display_login.innerHTML = response.users[key] + ' :';
								comment_display.appendChild(new_div_display_login);

								var new_p_display_comment = document.createElement('p');
								new_p_display_comment.innerHTML = response.comments[key];
								comment_display.appendChild(new_p_display_comment);
							}
						}
					}
				}
				else if (xmlhttp.status === 400)
				{
					console.log("There was an error 400");
				}
				else
				{
					console.log("Something else other than 200 was returned");
				}
			}
		};
		xmlhttp.open("POST", "src/display_comments.php", true);
		xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlhttp.send(form_datas);
	}

}

function close_modal_image()
{
    var button_delete = document.getElementById("button_delete");
	var button_like = document.getElementById("button_like");
	var comment_form = document.getElementById("comment_form");
	var comment_display = document.getElementById('comment_display');
	if (button_delete !== null && button_delete !== undefined) {
		button_delete.parentNode.removeChild(button_delete);
	}
	if (button_like !== null && button_like !== undefined) {
		button_like.parentNode.removeChild(button_like);
	}
	if (comment_form !== null && comment_form !== undefined) {
		comment_form.parentNode.removeChild(comment_form);
	}
	if (comment_display !== null && comment_display !== undefined) {
		comment_display.parentNode.removeChild(comment_display);
	}
	modal.classList.add("hidden");
}

function create_modal_image(gallery_image)
{
    var modal_options = document.getElementById("modal_options"),
	comment_part = document.getElementById('comment_part');
	
	image.setAttribute("src", gallery_image.firstElementChild.getAttribute("src"));
	image.setAttribute("id", "modal_" + gallery_image.firstElementChild.getAttribute("id"));
	image.addEventListener("click", close_modal_image);

	var xmlhttp = new XMLHttpRequest();
	var form_datas = "id_img=" + gallery_image.firstElementChild.getAttribute("id");

	if (xmlhttp != null && form_datas != null)
	{
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState === XMLHttpRequest.DONE)
			{
				if (xmlhttp.status === 200)
				{
					if (xmlhttp.responseText['success'] !== null)
					{
						var response = JSON.parse(xmlhttp.responseText);

						if (response.success === true) {
							if (response.user === true)
							{
								var new_button_like = document.createElement("button");
								new_button_like.innerHTML = "J'aime";
								new_button_like.setAttribute("id", "button_like");
								if (response.liked_by_user === true) {
									new_button_like.setAttribute("class", "button_form_liked");
								}
								else {
									new_button_like.setAttribute("class", "button_form_unliked");
								}
								modal_options.appendChild(new_button_like);
								
								like_image();
							}
							if (response.owner_image === true)
							{
								var new_button_delete = document.createElement("button");
								new_button_delete.innerHTML = "Supprimer";
								new_button_delete.setAttribute("id", "button_delete");
								new_button_delete.setAttribute("class", "button_form");
								modal_options.appendChild(new_button_delete);

								delete_image();
							}

							var new_div_display_comment = document.createElement('div');
							new_div_display_comment.setAttribute('id', 'comment_display');

							comment_part.appendChild(new_div_display_comment);
							display_comments();

							if (response.user === true) {
								var new_form_comment = document.createElement('form');
								new_form_comment.setAttribute('id', 'comment_form');

								var new_textarea = document.createElement('textarea');
								new_textarea.setAttribute('class', 'modal_comment');
								new_textarea.setAttribute('id', 'textarea_comment');
								new_form_comment.appendChild(new_textarea);

								var new_div_send = document.createElement('button');
								new_div_send.setAttribute('class', 'send');
								new_div_send.setAttribute('type', 'submit');
								new_form_comment.appendChild(new_div_send);

								var new_send_comment = document.createElement('i');
								new_send_comment.setAttribute('class', 'material_icons');
								new_send_comment.innerHTML = 'send';
								new_div_send.appendChild(new_send_comment);

								comment_part.appendChild(new_form_comment);
								post_comment();
							}

							var nb_likes = parseInt(response.nb_likes);
							document.getElementById("nb_likes").innerHTML = nb_likes.toString();
							document.getElementById('creator_img').innerHTML = response.login;
						}
					}
				}
				else if (xmlhttp.status === 400)
				{
					console.log("There was an error 400");
				}
				else
				{
					console.log("Something else other than 200 was returned");
				}
			}
		};
		xmlhttp.open("POST", "src/check_user_image.php", true);
		xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlhttp.send(form_datas);
	}
	modal.classList.remove("hidden");
}

infiniteScroll();
view_img();
