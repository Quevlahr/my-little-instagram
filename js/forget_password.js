function give_email()
{
	var error_msg = document.getElementById("error_give_email"),
	submit = document.getElementById("submit_form_email");

	if (submit != null && error_msg != null)
	{
		submit.addEventListener("click", function (event)
		{
			event.preventDefault();
			
			var xmlhttp = new XMLHttpRequest();
			var datas = document.querySelectorAll("#give_email .input_form");
			var form_datas = "";

			if (xmlhttp != null && datas != null && form_datas != null)
			{
				for (var i = 0; i < datas.length; i++)
				{
					form_datas += datas[i].name + "=" + datas[i].value;
					if (i != datas.length - 1)
						form_datas += "&";
				}
				xmlhttp.onreadystatechange = function()
				{
					if (xmlhttp.readyState == XMLHttpRequest.DONE)
					{
						if (xmlhttp.status == 200)
						{
							if (xmlhttp.responseText['message'] !== null)
							{
								var response = JSON.parse(xmlhttp.responseText);
								error_msg.innerHTML = response['message'];
							}
						}
						else if (xmlhttp.status == 400)
						{
							error_msg.innerHTML = "There was an error 400";
						}
						else
						{
							error_msg.innerHTML = "Something else other than 200 was returned";
						}
					}
				};
				xmlhttp.open("POST", "src/check_give_email.php", true);
				xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				xmlhttp.send(form_datas);
			}
		});
	}
}

give_email();