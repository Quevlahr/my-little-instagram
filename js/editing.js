(function()
{
	var streaming = false,
	video = document.querySelector('#video'),
	cover = document.querySelector('#cover'),
	canvas = document.querySelector('#canvas'),
	startbutton = document.querySelector('#startbutton'),
	width = 540,
	height = 0;

	navigator.getMedia = (	navigator.getUserMedia ||
							navigator.webkitGetUserMedia ||
							navigator.mozGetUserMedia ||
							navigator.msGetUserMedia);

	navigator.getMedia(
		{
			video: true,
			audio: false
		},
		function(stream)
		{
			if (navigator.mozGetUserMedia)
			{
				video.mozSrcObject = stream;
			}
			else
			{
				var vendorURL = window.URL || window.webkitURL;
				video.src = vendorURL.createObjectURL(stream);
			}
			video.play();
		},
		function(err)
		{
			console.log("An error occured! " + err);
		}
	);

	video.addEventListener('canplay', function(event)
	{
		if (!streaming)
		{
			height = video.videoHeight / (video.videoWidth/width);
			video.setAttribute('width', width);
			video.setAttribute('height', height);
			canvas.setAttribute('width', width);
			canvas.setAttribute('height', height);
			streaming = true;
		}
	}, false);

	function putpicture(response)
	{
		var all_image = document.getElementById("side_editing");

		new_div = document.createElement('div');
		new_div.setAttribute("class", "item_side_editing");
		new_image = document.createElement('img');
		new_image.setAttribute("src", response.path_img);
		new_image.setAttribute("id", response.id_img);
		new_image.setAttribute("alt", response.name_img);
		new_div.appendChild(new_image);

		var no_image = document.getElementById('no_image');
		
		if (no_image) {
			all_image.removeChild(no_image);
		}
		all_image.insertBefore(new_div, all_image.firstChild);
	}

	function takepicture(frame)
	{
		canvas.width = width;
		canvas.height = height;
		canvas.getContext('2d').drawImage(video, 0, 0, width, height);

		data = canvas.toDataURL('image/png');

		if (document.getElementById('input_file').value != "") {

			var xmlhttp = new XMLHttpRequest(),
			form = document.getElementById('frame_editing'),
			form_data = new FormData(form),
			error_msg = document.getElementById("error_img");

			if (xmlhttp != null && form_data != null) {
				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == XMLHttpRequest.DONE) {
						if (xmlhttp.status == 200) {
							if (xmlhttp.responseText.message !== null) {

								var response = JSON.parse(xmlhttp.responseText);
								error_msg.innerHTML = response.message;

								if (response.success === true) {
									putpicture(response);
								}
							}
						}
						else if (xmlhttp.status == 400) {
							error_msg.innerHTML = "There was an error 400";
						}
						else {
							error_msg.innerHTML = "Something else other than 200 was returned";
						}
					}
				};
				xmlhttp.open("POST", "src/create_image.php", true);
				xmlhttp.send(form_data);
			}
		}
		else {
			var xmlhttp = new XMLHttpRequest(),
			form_data = "data=" + data + '&frame=' + frame,
			error_msg = document.getElementById("error_img");

			if (xmlhttp != null && form_data != null) {
				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == XMLHttpRequest.DONE) {
						if (xmlhttp.status == 200) {
							if (xmlhttp.responseText.message !== null) {

								var response = JSON.parse(xmlhttp.responseText);
								error_msg.innerHTML = response.message;

								if (response.success === true) {
									putpicture(response);
								}
							}
						}
						else if (xmlhttp.status == 400) {
							error_msg.innerHTML = "There was an error 400";
						}
						else {
							error_msg.innerHTML = "Something else other than 200 was returned";
						}
					}
				};
				xmlhttp.open("POST", "src/create_image.php", true);
				xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				xmlhttp.send(form_data);
			}
		}
	}

	startbutton.addEventListener('click', function(event)
	{
		event.preventDefault();

		var radios = document.getElementsByName('frame');
		var frame = '';

		if (Array.from(radios).some(function(element) {
			if (element.checked) {
				frame = element.value;
				return true;
			}
		}) === true) {
			takepicture(frame);
		}
		else {
			document.getElementById('error_img').innerHTML = "Veuillez sélectionner un cadre.";
		}

	}, false);
})();
