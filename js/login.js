function check_login()
{
	var error_msg = document.getElementById("error_connexion"),
	submit = document.getElementById("submit_form_connexion");

	if (submit != null && error_msg != null)
	{
		submit.addEventListener("click", function (event)
		{
			event.preventDefault();
			
			var xmlhttp = new XMLHttpRequest();
			var datas = document.querySelectorAll("#login .input_form");
			var form_datas = "";

			if (xmlhttp != null && datas != null && form_datas != null)
			{
				for (var i = 0; i < datas.length; i++)
				{
					form_datas += datas[i].name + "=" + datas[i].value;
					if (i != datas.length - 1)
						form_datas += "&";
				}
				xmlhttp.onreadystatechange = function()
				{
					if (xmlhttp.readyState == XMLHttpRequest.DONE)
					{
						if (xmlhttp.status == 200)
						{
							if (xmlhttp.responseText['message'] !== null)
							{
								var response = JSON.parse(xmlhttp.responseText);
								if (response['success'] === true)
								{
									window.location.href = "index.php";
								}
								error_msg.innerHTML = response['message'];
							}
						}
						else if (xmlhttp.status == 400)
						{
							error_msg.innerHTML = "There was an error 400";
						}
						else
						{
							error_msg.innerHTML = "Something else other than 200 was returned";
						}
					}
				};
				xmlhttp.open("POST", "src/check_login.php", true);
				xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				xmlhttp.send(form_datas);
			}
		});
	}
}

check_login();

function change_card()
{
	var links = document.querySelectorAll(".change_card"),
	login_card = document.getElementById("login"),
	give_email_card = document.getElementById("give_email");

	if (links != null)
	{
		for (value of links)
		{
			value.addEventListener("click", function(event)
			{
				event.preventDefault();
				login_card.classList.toggle("hidden");
				give_email_card.classList.toggle("hidden");
			});
		}
	}

}

window.addEventListener("load", function(event) {
	change_card();
});