var image = document.querySelector(".no_gallery>img");

function like_image() {
	var button_like = document.getElementById("button_like");

	if (!button_like) {
		return ;
	}

	button_like.addEventListener("click", function()
	{
		var xmlhttp = new XMLHttpRequest();
		var id_img = image.getAttribute("id");
		var form_datas = "id_img=" + id_img;
		var nb_likes = document.getElementById("nb_likes");

		if ((xmlhttp !== null || xmlhttp !== undefined) && form_datas != null) {
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == XMLHttpRequest.DONE) {
					if (xmlhttp.status == 200) {
						if (xmlhttp.responseText['success'] !== null) {
							var response = JSON.parse(xmlhttp.responseText);
							if (response.success === true) {

								if (button_like.className == 'button_form_liked') {
									button_like.setAttribute('class', 'button_form_unliked');
								}
								else {
									button_like.setAttribute('class', 'button_form_liked');
								}

								var getNbLikes = new XMLHttpRequest();

								if (getNbLikes != null && form_datas != null) {
									getNbLikes.onreadystatechange = function() {
										if (getNbLikes.readyState == XMLHttpRequest.DONE) {
											if (getNbLikes.status == 200) {
												if (getNbLikes.responseText['success'] !== null) {
													var response = JSON.parse(getNbLikes.responseText);
													if (response['success'] === true) {
														nb_likes.innerHTML = parseInt(response['nb_likes']);
													}
												}
											}
											else if (getNbLikes.status == 400) {
												console.log("There was an error 400");
											}
											else {
												console.log("Something else other than 200 was returned");
											}
										}
									};
									getNbLikes.open("POST", "src/check_user_image.php", true);
									getNbLikes.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
									getNbLikes.send(form_datas);
								}


							}
						}

					}
					else if (xmlhttp.status == 400)
					{
						console.log("There was an error 400");
					}
					else
					{
						console.log("Something else other than 200 was returned");
					}
				}
			};
			xmlhttp.open("POST", "src/thumbs_up.php", true);
			xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xmlhttp.send(form_datas);
		}
	});
}


function delete_image() {
	var button_delete = document.getElementById("button_delete");

	if (!button_delete) {
		return ;
	}

	button_delete.addEventListener("click", function()
	{
		delete_confirmation = confirm("Êtes-vous sûr de vouloir supprimer cette photo ?");
		if (delete_confirmation === true)
		{
			var xmlhttp = new XMLHttpRequest();
			var id_img = image.getAttribute("id");
			var form_datas = "id_img=" + id_img;

			if (xmlhttp !== null && form_datas !== null)
			{
				xmlhttp.onreadystatechange = function()
				{
					if (xmlhttp.readyState == XMLHttpRequest.DONE)
					{
						if (xmlhttp.status == 200)
						{
							if (xmlhttp.responseText['success'] !== null)
							{
								var response = JSON.parse(xmlhttp.responseText);
								if (response.success === true)
								{
									alert(response.message);
									document.location.href="gallery.php";
								}
							}
						}
						else if (xmlhttp.status == 400)
						{
							console.log("There was an error 400");
						}
						else
						{
							console.log("Something else other than 200 was returned");
						}
					}
				};
				xmlhttp.open("POST", "src/delete_image.php", true);
				xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				xmlhttp.send(form_datas);
			}
		}
	});
}

function post_comment() {
	var comment_form = document.getElementById('comment_form');

	comment_form.addEventListener("submit", function(event)
	{
		event.preventDefault();

		var xmlhttp = new XMLHttpRequest();
		var id_img = image.getAttribute("id");
		var comment = document.getElementById('textarea_comment');
		var length_comment = comment.value.length;

		if (length_comment == 0 && length_comment > 300) {
			return;
		}

		var form_datas = "id_img=" + id_img + "&comment=" + comment.value;

		if (xmlhttp != null && form_datas != null)
		{
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == XMLHttpRequest.DONE)
				{
					if (xmlhttp.status == 200)
					{
						if (xmlhttp.responseText['success'] !== null)
						{
							var response = JSON.parse(xmlhttp.responseText);
							if (response.success === true) {
								display_comments();
								comment.value = '';
							}
						}
					}
					else if (xmlhttp.status == 400)
					{
						console.log("There was an error 400");
					}
					else
					{
						console.log("Something else other than 200 was returned");
					}
				}
			};
			xmlhttp.open("POST", "src/post_comment.php", true);
			xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xmlhttp.send(form_datas);
		}
	});
}

function display_comments() {

	var xmlhttp = new XMLHttpRequest();
	var id_img = image.getAttribute("id");
	var form_datas = "id_img=" + id_img;
	var comment_display = document.getElementById('comment_display');

	if (xmlhttp != null && form_datas != null)
	{
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == XMLHttpRequest.DONE)
			{
				if (xmlhttp.status == 200)
				{
					if (xmlhttp.responseText['success'] !== null)
					{
						var response = JSON.parse(xmlhttp.responseText);
						if (response.success === true) {
							while (comment_display.firstChild) {
								comment_display.removeChild(comment_display.firstChild);
							}
							for (key in response.comments) {
								var new_div_display_login = document.createElement('div');
								new_div_display_login.innerHTML = response.users[key] + ' :';
								comment_display.appendChild(new_div_display_login);

								var new_p_display_comment = document.createElement('p');
								new_p_display_comment.innerHTML = response.comments[key];
								comment_display.appendChild(new_p_display_comment);
							}
						}
					}
				}
				else if (xmlhttp.status == 400)
				{
					console.log("There was an error 400");
				}
				else
				{
					console.log("Something else other than 200 was returned");
				}
			}
		};
		xmlhttp.open("POST", "src/display_comments.php", true);
		xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xmlhttp.send(form_datas);
	}

}

post_comment();
like_image();
delete_image();
