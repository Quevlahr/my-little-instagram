function reset_password()
{
	var error_msg = document.getElementById("error_new_password"),
	submit = document.getElementById("submit_form_new_password");

	if (submit != null && error_msg != null)
	{
		submit.addEventListener("click", function (event)
		{
			event.preventDefault();
			
			var xmlhttp = new XMLHttpRequest();
			var datas = document.querySelectorAll("#new_password .input_form");
			var form_datas = "";

			if (xmlhttp != null && datas != null && form_datas != null)
			{
				for (var i = 0; i < datas.length; i++)
				{
					form_datas += datas[i].name + "=" + datas[i].value;
					if (i != datas.length - 1)
						form_datas += "&";
				}
				xmlhttp.onreadystatechange = function()
				{
					if (xmlhttp.readyState == XMLHttpRequest.DONE)
					{
						if (xmlhttp.status == 200)
						{
							if (xmlhttp.responseText['message'] !== null)
							{
								var response = JSON.parse(xmlhttp.responseText);
								if (response['success'] === true)
								{
									window.location.href = "index.php";
								}
								error_msg.innerHTML = response['message'];
							}
						}
						else if (xmlhttp.status == 400)
						{
							error_msg.innerHTML = "There was an error 400";
						}
						else
						{
							error_msg.innerHTML = "Something else other than 200 was returned";
						}
					}
				};
				xmlhttp.open("POST", "src/check_reset_password.php", true);
				xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				xmlhttp.send(form_datas);
			}
		});
	}
}

window.addEventListener('load', function(event) {
	reset_password();
});
