-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  jeu. 15 fév. 2018 à 07:54
-- Version du serveur :  5.7.20
-- Version de PHP :  7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_camagru`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments_camagru`
--

CREATE TABLE `comments_camagru` (
  `id_comment` int(10) UNSIGNED NOT NULL,
  `comment` varchar(300) NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_img` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comments_camagru`
--

INSERT INTO `comments_camagru` (`id_comment`, `comment`, `id_user`, `id_img`) VALUES
(1, 'coucou', 1, 164),
(11, '2', 1, 164),
(12, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nec sem lorem. Suspendisse ornare venenatis ante, posuere porttitor magna bibendum sit amet. Mauris tincidunt sed nibh sed mollis. Etiam iaculis vestibulum blandit. Duis mi quam, rhoncus quis lectus eu, pellentesque molestie sem. Morbi sed', 1, 164),
(13, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nec sem lorem. Suspendisse ornare venenatis ante, posuere porttitor magna bibendum sit amet. Mauris tincidunt sed nibh sed mollis. Etiam iaculis vestibulum blandit. Duis mi quam, rhoncus quis lectus eu, pellentesque molestie sem. Morbi sed.', 1, 164),
(15, 'yo', 1, 165),
(16, 'lol t\'es moche', 4, 164),
(17, 'coucou', 1, 165),
(18, 'Nouveau commentaire\n', 1, 166),
(19, 'Hello', 1, 166),
(20, 'nouveau commentaire', 1, 166),
(21, 'xD', 1, 164),
(22, 'De test\n', 4, 166),
(26, 'C\'est ma photo et je commente', 4, 283),
(27, 'Cette photo est nulle', 1, 283),
(28, 'hello', 1, 282),
(29, 'c vré', 1, 283),
(30, 'ygshdifjhsudhf oidhpfipdfgjpdofj opjdfpoj godjfpoj gpodjofj odjfgoj opdifjg jdopfjgpo jdopfjgpo idjfgjodjpgjdjfgojdpfgijodfjgodjfgodijg', 1, 283),
(31, '&lt;script&gt;alert(\'toto\')&lt;/script&gt;', 1, 283),
(32, 'ahah le kon\n', 1, 297),
(33, 'suepr', 1, 299),
(54, 'test d\'envoie de putain de mail', 4, 301),
(57, 'ahah', 1, 301),
(59, 'yop', 1, 301),
(63, 'hello', 1, 299),
(64, 'helllo', 1, 301),
(65, 'ahah', 17, 301),
(66, 'trop bien ca marche', 1, 306);

-- --------------------------------------------------------

--
-- Structure de la table `images_camagru`
--

CREATE TABLE `images_camagru` (
  `id_img` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `name_img` varchar(255) NOT NULL,
  `width_img` varchar(255) NOT NULL,
  `height_img` varchar(255) NOT NULL,
  `type_img` varchar(255) NOT NULL,
  `path_img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `images_camagru`
--

INSERT INTO `images_camagru` (`id_img`, `id_user`, `name_img`, `width_img`, `height_img`, `type_img`, `path_img`) VALUES
(1, 1, 'photo_59143afd2f8845.01223395.png', '540', '405', 'image/png', 'images/creation/photo_59143afd2f8845.01223395.png'),
(2, 1, 'photo_59143b0c4d5c31.09835576.png', '540', '405', 'image/png', 'images/creation/photo_59143b0c4d5c31.09835576.png'),
(3, 1, 'photo_59143b0d4f0dc2.48867881.png', '540', '405', 'image/png', 'images/creation/photo_59143b0d4f0dc2.48867881.png'),
(4, 1, 'photo_59143b0e7d52c3.17737798.png', '540', '405', 'image/png', 'images/creation/photo_59143b0e7d52c3.17737798.png'),
(5, 1, 'photo_59143b0f951db0.41379248.png', '540', '405', 'image/png', 'images/creation/photo_59143b0f951db0.41379248.png'),
(6, 1, 'photo_59143b1135b625.89353286.png', '540', '405', 'image/png', 'images/creation/photo_59143b1135b625.89353286.png'),
(7, 1, 'photo_59143b115ac866.30997770.png', '540', '405', 'image/png', 'images/creation/photo_59143b115ac866.30997770.png'),
(8, 1, 'photo_59143b11800028.40392961.png', '540', '405', 'image/png', 'images/creation/photo_59143b11800028.40392961.png'),
(9, 1, 'photo_59143b11a574b8.49011396.png', '540', '405', 'image/png', 'images/creation/photo_59143b11a574b8.49011396.png'),
(11, 1, 'photo_59143b12206219.58842090.png', '540', '405', 'image/png', 'images/creation/photo_59143b12206219.58842090.png'),
(12, 1, 'photo_59143b1245b4a7.44444968.png', '540', '405', 'image/png', 'images/creation/photo_59143b1245b4a7.44444968.png'),
(13, 1, 'photo_59143b1269cba3.65501656.png', '540', '405', 'image/png', 'images/creation/photo_59143b1269cba3.65501656.png'),
(14, 1, 'photo_59143ebba3f010.33806020.png', '540', '405', 'image/png', 'images/creation/photo_59143ebba3f010.33806020.png'),
(17, 1, 'photo_591455243fcfb5.36725766.png', '540', '405', 'image/png', 'images/creation/photo_591455243fcfb5.36725766.png'),
(18, 1, 'photo_591455246f2cd4.86301193.png', '540', '405', 'image/png', 'images/creation/photo_591455246f2cd4.86301193.png'),
(19, 1, 'photo_5914552497c933.99966168.png', '540', '405', 'image/png', 'images/creation/photo_5914552497c933.99966168.png'),
(20, 1, 'photo_59145524bf69b6.81136293.png', '540', '405', 'image/png', 'images/creation/photo_59145524bf69b6.81136293.png'),
(21, 1, 'photo_5914597203a570.92027497.png', '540', '405', 'image/png', 'images/creation/photo_5914597203a570.92027497.png'),
(22, 1, 'photo_591459731c9518.34136200.png', '540', '405', 'image/png', 'images/creation/photo_591459731c9518.34136200.png'),
(23, 1, 'photo_59145973885de3.13017038.png', '540', '405', 'image/png', 'images/creation/photo_59145973885de3.13017038.png'),
(24, 1, 'photo_5914597436cf28.02889159.png', '540', '405', 'image/png', 'images/creation/photo_5914597436cf28.02889159.png'),
(25, 1, 'photo_59145974659b22.52901252.png', '540', '405', 'image/png', 'images/creation/photo_59145974659b22.52901252.png'),
(26, 1, 'photo_591459748d3492.11544024.png', '540', '405', 'image/png', 'images/creation/photo_591459748d3492.11544024.png'),
(27, 1, 'photo_59145974be8c86.75289815.png', '540', '405', 'image/png', 'images/creation/photo_59145974be8c86.75289815.png'),
(28, 1, 'photo_59145974eb70e3.51555832.png', '540', '405', 'image/png', 'images/creation/photo_59145974eb70e3.51555832.png'),
(29, 1, 'photo_591459751c7e83.84888799.png', '540', '405', 'image/png', 'images/creation/photo_591459751c7e83.84888799.png'),
(30, 1, 'photo_59145975447a76.61099764.png', '540', '405', 'image/png', 'images/creation/photo_59145975447a76.61099764.png'),
(31, 1, 'photo_59145975700169.50535303.png', '540', '405', 'image/png', 'images/creation/photo_59145975700169.50535303.png'),
(32, 1, 'photo_591459759fa983.26409203.png', '540', '405', 'image/png', 'images/creation/photo_591459759fa983.26409203.png'),
(33, 1, 'photo_59145975ca2fc5.97957700.png', '540', '405', 'image/png', 'images/creation/photo_59145975ca2fc5.97957700.png'),
(34, 1, 'photo_591459760cfd06.25152095.png', '540', '405', 'image/png', 'images/creation/photo_591459760cfd06.25152095.png'),
(37, 1, 'photo_59145976d46198.89471385.png', '540', '405', 'image/png', 'images/creation/photo_59145976d46198.89471385.png'),
(39, 4, 'photo_5914890f757fc6.30893720.png', '540', '405', 'image/png', 'images/creation/photo_5914890f757fc6.30893720.png'),
(40, 4, 'photo_5914892dc7aea3.22931067.png', '540', '405', 'image/png', 'images/creation/photo_5914892dc7aea3.22931067.png'),
(41, 4, 'photo_5914892fcee6c4.79425008.png', '540', '405', 'image/png', 'images/creation/photo_5914892fcee6c4.79425008.png'),
(42, 4, 'photo_591495e756dbf8.33275113.png', '540', '405', 'image/png', 'images/creation/photo_591495e756dbf8.33275113.png'),
(43, 1, 'photo_5914ca972cb474.80444324.png', '540', '405', 'image/png', 'images/creation/photo_5914ca972cb474.80444324.png'),
(44, 1, 'photo_5914ca9aa47268.13223066.png', '540', '405', 'image/png', 'images/creation/photo_5914ca9aa47268.13223066.png'),
(46, 1, 'photo_5914ca9b9bf4a5.43824993.png', '540', '405', 'image/png', 'images/creation/photo_5914ca9b9bf4a5.43824993.png'),
(48, 1, 'photo_5914ca9e25ae02.98753374.png', '540', '405', 'image/png', 'images/creation/photo_5914ca9e25ae02.98753374.png'),
(49, 1, 'photo_5914ca9f6a8b24.94951441.png', '540', '405', 'image/png', 'images/creation/photo_5914ca9f6a8b24.94951441.png'),
(51, 1, 'photo_5914d6f92e6d83.49397704.png', '540', '405', 'image/png', 'images/creation/photo_5914d6f92e6d83.49397704.png'),
(52, 1, 'photo_5914d7ab03b359.42641433.png', '540', '405', 'image/png', 'images/creation/photo_5914d7ab03b359.42641433.png'),
(53, 1, 'photo_5914e4df4b37a4.31753562.png', '540', '405', 'image/png', 'images/creation/photo_5914e4df4b37a4.31753562.png'),
(54, 1, 'photo_5914e5069e6f21.31010569.png', '540', '405', 'image/png', 'images/creation/photo_5914e5069e6f21.31010569.png'),
(55, 1, 'photo_5914e58c763a13.66705893.png', '540', '405', 'image/png', 'images/creation/photo_5914e58c763a13.66705893.png'),
(56, 1, 'photo_5914e6efd67ac1.13094797.png', '540', '405', 'image/png', 'images/creation/photo_5914e6efd67ac1.13094797.png'),
(57, 1, 'photo_5914e709318d57.83670885.png', '540', '405', 'image/png', 'images/creation/photo_5914e709318d57.83670885.png'),
(58, 1, 'photo_5914ecccbc0842.91931058.png', '540', '405', 'image/png', 'images/creation/photo_5914ecccbc0842.91931058.png'),
(59, 1, 'photo_5914ee50cb1069.58905548.png', '540', '405', 'image/png', 'images/creation/photo_5914ee50cb1069.58905548.png'),
(60, 1, 'photo_5914ee76f078d4.10730089.png', '540', '405', 'image/png', 'images/creation/photo_5914ee76f078d4.10730089.png'),
(61, 1, 'photo_5914eec3e44241.32437758.png', '540', '405', 'image/png', 'images/creation/photo_5914eec3e44241.32437758.png'),
(64, 1, 'photo_5914efd99280d3.20753218.png', '540', '405', 'image/png', 'images/creation/photo_5914efd99280d3.20753218.png'),
(77, 1, 'photo_5914f886c84ed3.10403169.png', '540', '405', 'image/png', 'images/creation/photo_5914f886c84ed3.10403169.png'),
(84, 1, 'photo_5914fa7fd32f63.46603111.png', '540', '405', 'image/png', 'images/creation/photo_5914fa7fd32f63.46603111.png'),
(85, 1, 'photo_5914fa831d42f4.15912308.png', '540', '405', 'image/png', 'images/creation/photo_5914fa831d42f4.15912308.png'),
(86, 1, 'photo_5914fa8f26c343.47770211.png', '540', '405', 'image/png', 'images/creation/photo_5914fa8f26c343.47770211.png'),
(106, 1, 'photo_5914fbad504a61.22918793.png', '540', '405', 'image/png', 'images/creation/photo_5914fbad504a61.22918793.png'),
(107, 1, 'photo_5914fbaff06744.56049177.png', '540', '405', 'image/png', 'images/creation/photo_5914fbaff06744.56049177.png'),
(108, 1, 'photo_5914fbb20ff170.11131309.png', '540', '405', 'image/png', 'images/creation/photo_5914fbb20ff170.11131309.png'),
(109, 1, 'photo_5914fbb3db7646.30144600.png', '540', '405', 'image/png', 'images/creation/photo_5914fbb3db7646.30144600.png'),
(112, 1, 'photo_59152766c6ed72.90205032.png', '540', '405', 'image/png', 'images/creation/photo_59152766c6ed72.90205032.png'),
(140, 1, 'photo_591c661e6447b6.81328732.png', '540', '405', 'image/png', 'images/creation/photo_591c661e6447b6.81328732.png'),
(141, 1, 'photo_591c809406d7c2.21315621.png', '540', '405', 'image/png', 'images/creation/photo_591c809406d7c2.21315621.png'),
(144, 9, 'photo_592d64f1d227d6.97503418.png', '540', '405', 'image/png', 'images/creation/photo_592d64f1d227d6.97503418.png'),
(148, 9, 'photo_592d64f2b45e75.14967478.png', '540', '405', 'image/png', 'images/creation/photo_592d64f2b45e75.14967478.png'),
(164, 1, 'photo_59305d582fbfc2.05441984.png', '540', '405', 'image/png', 'images/creation/photo_59305d582fbfc2.05441984.png');

-- --------------------------------------------------------

--
-- Structure de la table `likes_camagru`
--

CREATE TABLE `likes_camagru` (
  `id_like` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_img` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `likes_camagru`
--

INSERT INTO `likes_camagru` (`id_like`, `id_user`, `id_img`) VALUES
(4, 1, 141),
(16, 1, 164),
(15, 1, 165),
(24, 1, 282),
(59, 1, 283),
(29, 1, 291),
(40, 1, 293),
(31, 1, 296),
(41, 1, 297),
(84, 1, 299),
(86, 1, 301),
(91, 1, 306),
(5, 4, 164),
(17, 4, 166),
(21, 4, 283),
(53, 17, 25),
(90, 17, 299),
(88, 17, 301);

-- --------------------------------------------------------

--
-- Structure de la table `users_camagru`
--

CREATE TABLE `users_camagru` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `reset_password` varchar(255) DEFAULT NULL,
  `confirm_inscription` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `notification` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(3) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users_camagru`
--

INSERT INTO `users_camagru` (`id_user`, `login`, `password`, `salt`, `reset_password`, `confirm_inscription`, `email`, `notification`, `access`) VALUES
(1, 'quroulon', 'a3f6431679010ef9d11463f874f8a35139a6cdde050cda0d406422a541dff5f8d0abfa6497b64ca06ecaa1c93aa0de12a4af021eafec1316c526206ee805b08e8e318070f4de0a05b282355d09183ed8ae617fb5e4b2513c961cbbfafbb1cee2', '8032599716769203928', NULL, '8032599716769203928', 'qroulon@gmail.com', 1, 10),
(4, 'test', 'ce40d7336b206ca9c396f4b01078cc58f71d9bb45c91a6485f6786aa4f30d579bb29a2da9cbaa1c80c6320960561ceedbc21af0c6c4ca5d17ef43963989dc8804a635090892bae5ab2eb69a359c3103703f57b47f0a66cf9ef504c92e46d557a', '2793862237272297243', NULL, '-3952715416763477723', 'test@gmail.com', 1, 1),
(17, 'test2', '2e491d365c796b860e65f91584a2f0778394995928a6f122eeffeeef36e684dabf7fae2fb1bf0e81ae8e7906132e6cec6c27dea4a6efb2d0a49e9c17c454a4a44bba9de1be8ef542803d30efd14340ab54391ebde401bd7c69e073f9e73c9160', '-7602500862981985282', NULL, '-6309719302075849393', 'test@test.com', 1, 1),
(19, 'qroulon', 'a3f6431679010ef9d11463f874f8a35139a6cdde050cda0d406422a541dff5f8d0abfa6497b64ca06ecaa1c93aa0de12a4af021eafec1316c526206ee805b08e8e318070f4de0a05b282355d09183ed8ae617fb5e4b2513c961cbbfafbb1cee2', '8032599716769203928', NULL, '8032599716769203928', 'qroulon@gmail.come', 1, 10);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comments_camagru`
--
ALTER TABLE `comments_camagru`
  ADD PRIMARY KEY (`id_comment`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_img` (`id_img`);

--
-- Index pour la table `images_camagru`
--
ALTER TABLE `images_camagru`
  ADD PRIMARY KEY (`id_img`),
  ADD UNIQUE KEY `ind_uni_name_img` (`name_img`),
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `likes_camagru`
--
ALTER TABLE `likes_camagru`
  ADD PRIMARY KEY (`id_like`),
  ADD UNIQUE KEY `ind_uni_like` (`id_user`,`id_img`),
  ADD KEY `id_img` (`id_img`);

--
-- Index pour la table `users_camagru`
--
ALTER TABLE `users_camagru`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `ind_uni_login` (`login`),
  ADD UNIQUE KEY `ind_uni_email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comments_camagru`
--
ALTER TABLE `comments_camagru`
  MODIFY `id_comment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT pour la table `images_camagru`
--
ALTER TABLE `images_camagru`
  MODIFY `id_img` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=310;

--
-- AUTO_INCREMENT pour la table `likes_camagru`
--
ALTER TABLE `likes_camagru`
  MODIFY `id_like` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT pour la table `users_camagru`
--
ALTER TABLE `users_camagru`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comments_camagru`
--
ALTER TABLE `comments_camagru`
  ADD CONSTRAINT `comments_camagru_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users_camagru` (`id_user`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_camagru_ibfk_2` FOREIGN KEY (`id_img`) REFERENCES `images_camagru` (`id_img`) ON DELETE CASCADE;

--
-- Contraintes pour la table `images_camagru`
--
ALTER TABLE `images_camagru`
  ADD CONSTRAINT `images_camagru_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users_camagru` (`id_user`) ON DELETE CASCADE;

--
-- Contraintes pour la table `likes_camagru`
--
ALTER TABLE `likes_camagru`
  ADD CONSTRAINT `likes_camagru_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users_camagru` (`id_user`) ON DELETE CASCADE,
  ADD CONSTRAINT `likes_camagru_ibfk_2` FOREIGN KEY (`id_img`) REFERENCES `images_camagru` (`id_img`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
