<?php

	include("config.php");
	
	try {
		$bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
	}
	catch (Exception $e) {
		die('Erreur : ' . $e->getMessage());
	}

	$bdd->query("CREATE DATABASE IF NOT EXISTS ".$DB_NAME);
	$bdd->query("USE ".$DB_NAME);

	$bdd->query(
		"CREATE TABLE IF NOT EXISTS ".$DB_NAME.".".$DB_TABLE_USERS." (
		id_user INT UNSIGNED NOT NULL AUTO_INCREMENT,
		login varchar(255) NOT NULL,
		password varchar(255) NOT NULL,
		salt varchar(255) NOT NULL,
		reset_password varchar(255),
		confirm_inscription varchar(255) NOT NULL,
		email varchar(255) DEFAULT NULL,
		notification boolean NOT NULL DEFAULT 1,
		access TINYINT UNSIGNED DEFAULT 1 NOT NULL,
		PRIMARY KEY (id_user),
		UNIQUE KEY ind_uni_login (login),
		UNIQUE KEY ind_uni_email (email))
		ENGINE=InnoDB DEFAULT CHARSET=utf8;"
	);

	$bdd->query(
		"CREATE TABLE IF NOT EXISTS ".$DB_NAME.".".$DB_TABLE_IMAGES." (
		id_img INT UNSIGNED NOT NULL AUTO_INCREMENT,
		id_user INT UNSIGNED NOT NULL,
		name_img VARCHAR(255) NOT NULL,
		width_img VARCHAR(255) NOT NULL,
		height_img VARCHAR(255) NOT NULL,
		type_img VARCHAR(255) NOT NULL,
		path_img VARCHAR(255) NOT NULL,
		PRIMARY KEY (id_img),
		FOREIGN KEY (id_user) REFERENCES ".$DB_TABLE_USERS."(id_user) ON DELETE CASCADE,
		UNIQUE KEY ind_uni_name_img (name_img))
		ENGINE=InnoDB DEFAULT CHARSET=utf8;"
	);

	$bdd->query(
		"CREATE TABLE IF NOT EXISTS ".$DB_NAME.".".$DB_TABLE_LIKES." (
		id_like INT UNSIGNED NOT NULL AUTO_INCREMENT,
		id_user INT UNSIGNED NOT NULL,
		id_img INT UNSIGNED NOT NULL,
		PRIMARY KEY (id_like),
		FOREIGN KEY (id_user) REFERENCES ".$DB_TABLE_USERS."(id_user) ON DELETE CASCADE,
		FOREIGN KEY (id_img) REFERENCES ".$DB_TABLE_IMAGES."(id_img) ON DELETE CASCADE,
		UNIQUE KEY ind_uni_like (id_user, id_img))
		ENGINE=InnoDB DEFAULT CHARSET=utf8;"
	);

	$bdd->query(
		"CREATE TABLE IF NOT EXISTS ".$DB_NAME.".".$DB_TABLE_COMMENTS." (
		id_comment INT UNSIGNED NOT NULL AUTO_INCREMENT,
		comment VARCHAR(500) NOT NULL,
		id_user INT UNSIGNED NOT NULL,
		id_img INT UNSIGNED NOT NULL,
		PRIMARY KEY (id_comment),
		FOREIGN KEY (id_user) REFERENCES ".$DB_TABLE_USERS."(id_user) ON DELETE CASCADE,
		FOREIGN KEY (id_img) REFERENCES ".$DB_TABLE_IMAGES."(id_img) ON DELETE CASCADE)
		ENGINE=InnoDB DEFAULT CHARSET=utf8;"
	);

	try {
        $bdd->query("INSERT IGNORE INTO ".$DB_NAME.".".$DB_TABLE_USERS." VALUES (NULL, \"quroulon\", \"a3f6431679010ef9d11463f874f8a35139a6cdde050cda0d406422a541dff5f8d0abfa6497b64ca06ecaa1c93aa0de12a4af021eafec1316c526206ee805b08e8e318070f4de0a05b282355d09183ed8ae617fb5e4b2513c961cbbfafbb1cee2\", \"8032599716769203928\", NULL, \"8032599716769203928\", \"quroulon@gmail.come\", 1, 10);");
    }
    catch (Exception $e) {
	    echo $e->getMessage();
    }

?>

<!DOCTYPE html>
<html>
<head>
    <title>Camagru</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="icon" type="image/jpeg" href="/camagru/images/favicon.jpg" />

    <link rel="stylesheet" type="text/css" href="/camagru/css/fonts.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/header.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/footer.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/content.css" />

    <script type="text/javascript" src="/camagru/js/header.js" async ></script>
</head>
<body>

<div id="all">
    <div id="header">
        <div class="menu_item left_header">
            <a class="header_link first_link" href="/camagru/index.php" title="Camagru">Camagru</a>
        </div>
    </div>
    <div id="content">
        <div id="marge"></div>
    </div>
    <?php include("../src/footer.php"); ?>
</div>

</body>
</html>


?>

<!DOCTYPE html>
<html>
	<head>
		<title>Camagru</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<link rel="icon" type="image/jpeg" href="/camagru/images/favicon.jpg" />

		<link rel="stylesheet" type="text/css" href="/camagru/css/fonts.css" />
		<link rel="stylesheet" type="text/css" href="/camagru/css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="/camagru/css/header.css" />
		<link rel="stylesheet" type="text/css" href="/camagru/css/footer.css" />
		<link rel="stylesheet" type="text/css" href="/camagru/css/content.css" />

		<script type="text/javascript" src="/camagru/js/header.js" async ></script>
	</head>
	<body>

		<div id="all">
			<div id="header">
				<div class="menu_item left_header">
					<a class="header_link first_link" href="/camagru/index.php" title="Camagru">Camagru</a>
				</div>
			</div>
			<div id="content">
				<div id="marge"></div>
			</div>
			<?php include("../src/footer.php"); ?>
		</div>

	</body>
</html>