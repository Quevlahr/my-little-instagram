-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : qroulonfgwqr.mysql.db
-- Généré le :  jeu. 15 fév. 2018 à 18:16
-- Version du serveur :  5.6.34-log
-- Version de PHP :  5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `qroulonfgwqr`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments_camagru`
--

CREATE TABLE `comments_camagru` (
  `id_comment` int(10) UNSIGNED NOT NULL,
  `comment` varchar(500) NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_img` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comments_camagru`
--

INSERT INTO `comments_camagru` (`id_comment`, `comment`, `id_user`, `id_img`) VALUES
(1, 'trop b1', 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `images_camagru`
--

CREATE TABLE `images_camagru` (
  `id_img` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `name_img` varchar(255) NOT NULL,
  `width_img` varchar(255) NOT NULL,
  `height_img` varchar(255) NOT NULL,
  `type_img` varchar(255) NOT NULL,
  `path_img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `images_camagru`
--

INSERT INTO `images_camagru` (`id_img`, `id_user`, `name_img`, `width_img`, `height_img`, `type_img`, `path_img`) VALUES
(3, 1, 'photo_5a85c00f297bc0.55154291.png', '540', '405', 'image/png', 'images/creation/photo_5a85c00f297bc0.55154291.png');

-- --------------------------------------------------------

--
-- Structure de la table `likes_camagru`
--

CREATE TABLE `likes_camagru` (
  `id_like` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_img` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `likes_camagru`
--

INSERT INTO `likes_camagru` (`id_like`, `id_user`, `id_img`) VALUES
(2, 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `users_camagru`
--

CREATE TABLE `users_camagru` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `reset_password` varchar(255) DEFAULT NULL,
  `confirm_inscription` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `notification` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(3) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users_camagru`
--

INSERT INTO `users_camagru` (`id_user`, `login`, `password`, `salt`, `reset_password`, `confirm_inscription`, `email`, `notification`, `access`) VALUES
(1, 'quroulon', 'a3f6431679010ef9d11463f874f8a35139a6cdde050cda0d406422a541dff5f8d0abfa6497b64ca06ecaa1c93aa0de12a4af021eafec1316c526206ee805b08e8e318070f4de0a05b282355d09183ed8ae617fb5e4b2513c961cbbfafbb1cee2', '8032599716769203928', NULL, '8032599716769203928', 'qroulon@gmail.com', 1, 10);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comments_camagru`
--
ALTER TABLE `comments_camagru`
  ADD PRIMARY KEY (`id_comment`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_img` (`id_img`);

--
-- Index pour la table `images_camagru`
--
ALTER TABLE `images_camagru`
  ADD PRIMARY KEY (`id_img`),
  ADD UNIQUE KEY `ind_uni_name_img` (`name_img`),
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `likes_camagru`
--
ALTER TABLE `likes_camagru`
  ADD PRIMARY KEY (`id_like`),
  ADD UNIQUE KEY `ind_uni_like` (`id_user`,`id_img`),
  ADD KEY `id_img` (`id_img`);

--
-- Index pour la table `users_camagru`
--
ALTER TABLE `users_camagru`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `ind_uni_login` (`login`),
  ADD UNIQUE KEY `ind_uni_email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comments_camagru`
--
ALTER TABLE `comments_camagru`
  MODIFY `id_comment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `images_camagru`
--
ALTER TABLE `images_camagru`
  MODIFY `id_img` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `likes_camagru`
--
ALTER TABLE `likes_camagru`
  MODIFY `id_like` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `users_camagru`
--
ALTER TABLE `users_camagru`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comments_camagru`
--
ALTER TABLE `comments_camagru`
  ADD CONSTRAINT `comments_camagru_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users_camagru` (`id_user`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_camagru_ibfk_2` FOREIGN KEY (`id_img`) REFERENCES `images_camagru` (`id_img`) ON DELETE CASCADE;

--
-- Contraintes pour la table `images_camagru`
--
ALTER TABLE `images_camagru`
  ADD CONSTRAINT `images_camagru_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users_camagru` (`id_user`) ON DELETE CASCADE;

--
-- Contraintes pour la table `likes_camagru`
--
ALTER TABLE `likes_camagru`
  ADD CONSTRAINT `likes_camagru_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users_camagru` (`id_user`) ON DELETE CASCADE,
  ADD CONSTRAINT `likes_camagru_ibfk_2` FOREIGN KEY (`id_img`) REFERENCES `images_camagru` (`id_img`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
