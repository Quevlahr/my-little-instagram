<?php
	include("config.php");

	try
	{
		$bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		$bdd->query("USE ".$DB_NAME);
	}
	catch (Exception $e)
	{
		die('Erreur : ' . $e->getMessage());
	}

?>