<?php
	function check_reset_password()
	{
		include("config/database.php");
		
		$login = $_GET['login'];
		$key = $_GET['key'];

		try
		{
			$request_reset = $bdd->prepare("SELECT reset_password, login, email, id_user, access FROM ".$DB_NAME.".".$DB_TABLE_USERS." WHERE login like :login ");
			$request_reset->execute(array(
				':login' => $login));
		}
		catch (Exception $e)
		{
			return false;
		}

		$user = $request_reset->fetchAll()[0];
		$count = $request_reset->rowCount();
		if ($count == 1 && $user['reset_password'] == $key)
		{
			$_SESSION['id_user'] = $user['id_user'];
			$_SESSION['access'] = $user['access'];
			$_SESSION['connect'] = false;
			$_SESSION['login'] = $user['login'];
			$_SESSION['email'] = $user['email'];
			$request_reset->closeCursor();
			return true;
		}
		else
		{
			$request_reset->closeCursor();
			return false;
		}
	}

	include("src/session_non_connected.php");
	$accept_reset = false;
	if (!isset($_GET['login']) || !isset($_GET['key']) ||
		strlen($_GET['login']) < 4 || 
		strlen($_GET['login']) >= 255 ||
		$_GET['key'] == "")
	{
		header("location: error.php");
	}
	else if (check_reset_password() === false)
	{
		header("location: error.php");
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("src/head_html.php"); ?>
		<link rel="stylesheet"  type="text/css" href="css/formulaire.css" />
	</head>
	<body>

		<div id="all">
			<?php include("src/header.php"); ?>
			
			<div id="content">
				<div class="card" id="new_password">
					<div class="card_title">
						<h1>Nouveau mot de passe</h1>
					</div>
					<div class="container">
						<div>
							<p class="error_msg" id="error_new_password">Veuillez indiquer votre nouveau mot de passe.</p>
						</div>
						<form id="forget_new_password" class="form_connexion" method="post">
							<fieldset>
								<div class="input_group">
									<input class="input_form" name="password" type="password" required>
									<span class="input_bar"></span>
									<label for="password">Mot de passe</label>
								</div>
								<div class="input_group">
									<input class="input_form" name="check_password" type="password" required>
									<span class="input_bar"></span>
									<label for="password">Confirmation mot de passe</label>
								</div>
								<div class="button_group">
									<button id="submit_form_new_password" class="button_form" name="send" type="submit">Confirmer</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>

				<div id="marge"></div>
			</div>

			<?php include("src/footer.php"); ?>
		</div>

		<script type="text/javascript" src="js/reset_password.js"></script>

	</body>
</html>