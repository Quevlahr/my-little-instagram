<?php
	include("src/session_non_limited.php");
	include("config/database.php");
	try
	{
		if (isset($_SESSION['connect']) && $_SESSION['connect'] === true &&
			isset($_SESSION['access']) && $_SESSION['access'] !== NON_REGISTERED_ACCESS)
		{
			$req_sql = $bdd->query(
				"SELECT * 
				FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." 
				WHERE id_user = ".$_SESSION['id_user']." 
				ORDER BY id_img DESC LIMIT 20 OFFSET 0");
		}
		else
		{
			$req_sql = $bdd->query("SELECT id_img, path_img, name_img FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." ORDER BY id_img DESC LIMIT 20 OFFSET 0");
		}
	}
	catch (Exception $e)
	{
		exit($e->getMessage());
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("src/head_html.php"); ?>
		<link rel="stylesheet"  type="text/css" href="css/formulaire.css" />
		<link rel="stylesheet"  type="text/css" href="css/gallery.css" />
	</head>
	<body>
		<div id="all">
			<?php include("src/header.php"); ?>
			<div id="content">
				<div id="modal_gallery" class="modal hidden">
					<div class="modal_content">
						<p id="img_take_by">Photo prise par : <span id="creator_img"></span></p>
						<span class="close">&times;</span>
						<img class="modal_image" src="">
						<p>
							<div class=" thumb_up">
								<span id="nb_likes">0</span>
								<i class="material_icons">thumb_up</i>
							</div>
						</p>
						<div id="modal_options">
						</div>
						<div id="comment_part"></div>
					</div>
				</div>
				<?php
					if (isset($_SESSION['connect']) && $_SESSION['connect'] === true &&
						isset($_SESSION['access']) && $_SESSION['access'] !== NON_REGISTERED_ACCESS)
					{
						echo "<h1>Bienvenue ".$_SESSION['login'].".</h1>";
				?>

				<?php
						if ($req_sql->rowCount() != 0)
						{
				?>
							<p>Voici les derniers montages que vous avez créés !</p>
							<div id="unique_gallery" class="gallery">
				<?php
							while($image = $req_sql->fetch())
							{
								echo "<div class=\"item_gallery\">
									<a class=\"link_image\" href=\"gallery.php?id_img=".$image['id_img']."\" title=\"\">
										<img id=\"".$image['id_img']."\"src=\"".$image['path_img']."\" alt=\"".$image['name_img']."\">
									</a>
								</div>";
							}
				?>
							</div>
							<p id="load_infinite_scroll"></p>
				<?php
						}
						else
						{
				?>
							<p>Vous n'avez pas encore créé de montage allez sur la page "montage" pour commencer !</p>
				<?php
						}
					}
					else if (isset($_SESSION['connect']) && $_SESSION['connect'] === true &&
							isset($_SESSION['access']) && $_SESSION['access'] === NON_REGISTERED_ACCESS)
					{
				?>
						<h1>Vous devez activer votre compte en suivant les instructions que vous avez reçus par mail.</h1>
				<?php
					}
					else
					{
				?>
						<h1>Bienvenue sur Camagru.</h1>
				<?php
						if ($req_sql->rowCount() != 0)
						{
							echo "<p>Voici les ".$req_sql->rowCount()." derniers montages créés par d'autres utilisateurs.</p>";
				?>
							<div class="gallery">
				<?php
							while($image = $req_sql->fetch())
							{
								echo "<div class=\"item_gallery\"><img id=\"".$image['id_img']."\"src=\"".$image['path_img']."\" alt=\"".$image['name_img']."\"></div>";
							}
				?>
							</div>
				<?php
						}
					}
				?> 
				<div id="marge"></div>
			</div>
			<?php include("src/footer.php"); ?>
		</div>

		<?php
			if (isset($_SESSION['connect']) && $_SESSION['connect'] === true &&
				isset($_SESSION['access']) && $_SESSION['access'] !== NON_REGISTERED_ACCESS) {
		?>
		<script type="text/javascript"> var index = 1; </script>
		<script type="text/javascript" src="js/gallery.js"></script>
		<?php } ?>
	</body>
</html>
