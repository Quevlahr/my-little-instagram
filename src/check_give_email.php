<?php
	$response = array('success' => false, 'message' => "Une erreur est survenue. Veuillez réessayer.");
	if (isset($_POST['email']))
	{
		$email = $_POST['email'];
		if ($email === "")
		{
			$response['message'] = "Veuillez indiquer votre adresse mail.";
			exit (json_encode($response));
		}
		else
		{
			session_start();
			include("../config/database.php");

			try
			{
				$req_sql = $bdd->prepare("SELECT * FROM ".$DB_NAME.".".$DB_TABLE_USERS." WHERE email = :email");

				$req_sql->execute(array(
					':email' => $email));
			}
			catch (Exception $e)
			{
				$response['message'] = $e->getMessage();
				exit (json_encode($response));
			}

			// $user = $req_sql->fetchAll()[0];
			$user = $req_sql->fetch();
			$count = $req_sql->rowCount();
			// $response['message'] = $user['email'];
			// exit(json_encode($response));


			if ($count == 1 && $user['email'] === $email)
			{
				$reset_password = random_int(PHP_INT_MIN, PHP_INT_MAX);
				$req_sql->closeCursor();
				try
				{
					$req_sql = $bdd->prepare("UPDATE ".$DB_NAME.".".$DB_TABLE_USERS." SET reset_password = :reset_password WHERE email = :email");

					$req_sql->execute(array(
						':reset_password' => $reset_password,
						':email' => $user['email']));
				}
				catch (Exception $e)
				{
					$response['message'] = $e->getMessage();
					exit (json_encode($response));	
				}
				
				include("send_email.php");
				if (send_email_reset($user['email'], $user['login'], $reset_password)) {
					$response['success'] = true;
					$response['message'] = "Un email vous a été envoyé pour réinitialiser votre mot de passe.";
					exit (json_encode($response));					
				}

				$response['message'] = "L'email n'a pas pu être envoyé. Veuillez réessayer.";
				exit(json_encode($response));
			}
			else
			{
				$response['message'] = "Adresse mail incorrecte.";
				$req_sql->closeCursor();
				exit (json_encode($response));				
			}
		}
	}
	else
	{
		exit(json_encode($response));
	}
?>
