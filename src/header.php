<div id="header">
	<div class="menu_item left_header">
		<a class="header_link first_link" href="index.php" title="Camagru">Camagru</a>
	</div>
	<div class="menu_item middle_header">
		<a class="header_link" href="gallery.php" title="Galerie">Galerie</a>
		<a class="header_icon" href="gallery.php" title="Galerie">
			<i class="material_icons">photo_library</i>
		</a>
	<?php 
		if (isset($_SESSION['connect']) && $_SESSION['connect'] === true &&
			isset($_SESSION['access']) && ($_SESSION['access'] === BASIC_ACCESS ||
			$_SESSION['access'] === ADMIN_ACCESS)):
	?>
			<a class="header_link" href="editing.php" title="Montage">Montage</a>
			<a class="header_icon" href="editing.php" title="Montage">
				<i class="material_icons">photo_camera</i>
			</a>
		</div>
		<div class="menu_item right_header">
			<a class="header_link" href="profile.php" title="Profil">Mon Profil</a>
			<a class="header_icon" href="profile.php" title="Profil">
				<i class="material_icons">account_circle</i>
			</a>
			<a class="header_link" href="logout.php" title="Déconnexion">Déconnexion</a>
			<a class="header_icon" href="logout.php" title="Déconnexion">
				<i class="material_icons">highlight_off</i>
			</a>
		</div>
	<?php
		elseif (isset($_SESSION['connect']) && $_SESSION['connect'] === true &&
				isset($_SESSION['access']) && $_SESSION['access'] === NON_REGISTERED_ACCESS):
	?>
		</div>
		<div class="menu_item right_header">
			<a class="header_link" href="logout.php" title="Déconnexion">Déconnexion</a>
			<a class="header_icon" href="logout.php" title="Déconnexion">
				<i class="material_icons">highlight_off</i>
			</a>
		</div>
	<?php
		else:
	?>		
		</div>
		<div class="menu_item right_header">
			<a class="header_link" href="login.php" title="Connexion">Connexion</a>
			<a class="header_icon" href="login.php" title="Connexion">
				<i class="material_icons">power_settings_new</i>
			</a>
			<a class="header_link" href="register.php" title="Inscription">Inscription</a>
			<a class="header_icon" href="register.php" title="Inscription">
				<i class="material_icons">add_circle_outline</i>
			</a>
		</div>
	<?php
		endif;
	?>
</div>