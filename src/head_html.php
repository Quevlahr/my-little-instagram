<title>Camagru</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<link rel="icon" type="image/jpeg" href="/camagru/images/favicon.jpg" />

<link rel="stylesheet"  type="text/css" href="css/fonts.css" />
<link rel="stylesheet"  type="text/css" href="css/normalize.css" />
<link rel="stylesheet"  type="text/css" href="css/header.css" />
<link rel="stylesheet"  type="text/css" href="css/footer.css" />
<link rel="stylesheet"  type="text/css" href="css/content.css" />

<script type="text/javascript" src="js/header.js" async ></script>