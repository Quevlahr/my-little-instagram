<?php
	function validate_email ($email)
	{
		if (filter_var($email, FILTER_VALIDATE_EMAIL))
			return true;
		return false;
	}

	function validate_password ($password)
	{
		if (strlen($password) >= 6)
			return (true);
		return (false);
	}

	function validate_login ($login)
	{
		if (strlen($login) >= 4)
			return (true);
		return (false);
	}

	$response = array('success' => false, 'message' => "Une erreur est survenue. Veuillez réessayer.");
	if (isset($_POST['login']) &&
		isset($_POST['email']) &&
		isset($_POST['check_email']) &&
		isset($_POST['password']) &&
		isset($_POST['check_password']))
	{
		$login = htmlspecialchars($_POST['login']);
		$email = $_POST['email'];
		$check_email = $_POST['check_email'];
		$password = $_POST['password'];
		$check_password = $_POST['check_password'];

		if ($login === "" || $email === "" || $check_email === "" || $password === "" || $check_password === "")
		{
			$response['message'] = "Tous les champs sont nécessaires.";
			exit (json_encode($response));
		}
		else if (validate_email($email) === false)
		{
			$response['message'] = "Votre adresse email n'es pas correctement formaté.";
			exit (json_encode($response));
		}
		else if ($email !== $check_email)
		{
			$response['message'] = "La confirmation de l'email ne correspond pas.";
			exit (json_encode($response));
		}
		else if (strlen($login) >= 255)
		{
			$response['message'] = "Le login est trop long.";
			exit (json_encode($response));
		}
		else if (validate_login($login) === false)
		{
			$response['message'] = "Le login doit contenir au moins 4 caractères.";
			exit (json_encode($response));
		}
		else if (strlen($password) >= 255)
		{
			$response['message'] = "Le mot de passe est trop long.";
			exit (json_encode($response));
		}
		else if (validate_password($password) === false)
		{
			$response['message'] = "Votre mot de passe doit contenir au moins 6 caractères.";
			exit (json_encode($response));
		}
		else if ($password !== $check_password)
		{
			$response['message'] = "La confirmation du mot de passe ne correspond pas.";
			exit (json_encode($response));
		}
		else
		{
			session_start();
			include("../config/database.php");
			include("variables.php");
			include("encrypt_password.php");

			$salt = random_int(PHP_INT_MIN, PHP_INT_MAX);
			$confirm_inscription = random_int(PHP_INT_MIN, PHP_INT_MAX);
			$reset_password = null;
			$crypted_password = encrypt_password($password, $salt);
			try 
			{
				$req_sql = $bdd->prepare("INSERT INTO ".$DB_NAME.".".$DB_TABLE_USERS." VALUES (NULL, :login, :password, :salt, :reset_password, :confirm_inscription, :email, :notification, :access)");
				$req_sql->execute(array(
					':login' => $login,
					':password' => $crypted_password,
					':salt' => $salt,
					':reset_password' => $reset_password,
					':confirm_inscription' => $confirm_inscription,
					':email' => $email,
					':access' => NON_REGISTERED_ACCESS,
					':notification' => true
				));
			}
			catch (Exception $e)
			{
				$regex_sql_error = "/^SQLSTATE[23000]*/";
				$regex_sql_error_uni_email = "/.*".$email.".*/";
				$regex_sql_error_uni_login = "/.*".$login.".*/";
				if (preg_match($regex_sql_error, $e->getMessage()) === 1)
				{
					if (preg_match($regex_sql_error_uni_email, $e->getMessage()) === 1)
						$response['message'] = "Cette adresse mail est déjà utilisé.";
					else if (preg_match($regex_sql_error_uni_login, $e->getMessage()) === 1)
						$response['message'] = "Ce login est déjà utilisé.";
					else
						$response['message'] = $e->getMessage();
				}
				else
				{
					$response['message'] = $e->getMessage();
				}
				exit (json_encode($response));
			}

			$recup_id = $bdd->prepare("SELECT id_user FROM ".$DB_TABLE_USERS." WHERE login = :login");
			$recup_id->execute(array(':login' => $login));

			$id_user = $recup_id->fetch();
			$_SESSION['id_user'] = $id_user['id_user'];
			$_SESSION['connect'] = true;
			$_SESSION['login'] = $login;
			$_SESSION['email'] = $email;
			$_SESSION['access'] = NON_REGISTERED_ACCESS;
			$_SESSION['notification'] = true;

			include("send_email.php");
			if (send_email_inscription($email, $login, $confirm_inscription) === false)
			{
				$response['message'] = "Le mail n'a pas pu être envoyé.";
				exit(json_encode($response));
			}

			$recup_id->closeCursor();
			$response['success'] = true;
			$response['message'] = "Pour finaliser l'inscription veuillez confirmer votre adresse email.";
			exit (json_encode($response));	
		}
	}
	else
	{
		exit (json_encode($response));
	}
?>