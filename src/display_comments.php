<?php
	session_start();

	$response = array('success' => false, 'message' => "Une erreur est survenue. Veuillez réessayer!");

	if (!isset($_POST['id_img'])) {
		$response['message'] = "La variable id_img n'es pas set.";
		exit(json_encode($response));
	}

	$id_img = (int) $_POST['id_img'];
	if (!ctype_digit($_POST['id_img']) || $id_img < 0) {
		$response['message'] = "La variable id_img n'es pas un digit.";

		exit(json_encode($response));
	}

	include('../config/database.php');
	try {
		$req_sql = $bdd->prepare("SELECT comment, login 
			FROM ".$DB_NAME.".".$DB_TABLE_COMMENTS." 
			INNER JOIN ".$DB_NAME.".".$DB_TABLE_USERS." 
			ON ".$DB_TABLE_COMMENTS.".id_user = ".$DB_TABLE_USERS.".id_user 
			WHERE id_img = :id_img ORDER BY id_comment DESC");
		$req_sql->execute([
			':id_img' => $id_img
		]);
	}
	catch (Exception $e) {
		$response['message'] = "La requête n'a pas fonctionné";
		exit(json_encode($response));
	}

	$response['users'] = array();
	$response['comments'] = array();
	while ($comment = $req_sql->fetch()) {
		$response['users'][] = $comment['login'];
		$response['comments'][] = $comment['comment'];
	}

	$response['success'] = true;
	$response['message'] = "Les commentaires ont bien été recupérés.";

	exit(json_encode($response));
?>