<?php

	$response = array('success' => false, 'message' => "Une erreur est survenue. Veuillez réessayer.");
	if (isset($_POST['login']) && isset($_POST['password']))
	{
		$login = $_POST['login'];
		$password = $_POST['password'];
		if ($password === "" && $login === "")
		{
			$response['message'] = "Veuillez indiquer votre login ainsi que votre mot de passe.";
			exit (json_encode($response));
		}
		else if ($password === "")
		{
			$response['message'] = "Vous n'avez pas indiqué votre mot de passe.";
			exit (json_encode($response));
		}
		else if ($login === "")
		{
			$response['message'] = "Vous n'avez pas indiqué votre login.";
			exit (json_encode($response));
		}
		else
		{
			session_start();
			include("../config/database.php");
			include("encrypt_password.php");

			try
			{
				$req_sql = $bdd->prepare("SELECT * FROM ".$DB_NAME.".".$DB_TABLE_USERS." WHERE login = :login");
				$req_sql->execute(array(
					':login' => $login));

			}
			catch (Exception $e)
			{
				$response['message'] = $e->getMessage();
				exit (json_encode($response));
			}
			$user = $req_sql->fetch();
			$count = $req_sql->rowCount();
			
			if ($count == 1 && $user['login'] === $login && encrypt_password($password, $user['salt']) === $user['password'])
			{
				$_SESSION['id_user'] = $user['id_user'];
				$_SESSION['email'] = $user['email'];
				$_SESSION['login'] = $user['login'];
				$_SESSION['access'] = intVal($user['access']);
				$_SESSION['connect'] = true;
				$_SESSION['notification'] = boolval($user['notification']);

				$response['success'] = true;
				$response['message'] = "Vous êtes maintenant connecté.";

				$req_sql->closeCursor();
				exit (json_encode($response));
			}
			else
			{
				$response['message'] = "Identifiants incorrects, veuillez vérifier votre login ou votre mot de passe.";
				$req_sql->closeCursor();
				exit (json_encode($response));				
			}
		}
	}
	else
	{
		exit(json_encode($response));
	}
?>
