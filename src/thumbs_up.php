<?php
	session_start();

	$response = array('success' => false, 'message' => "Une erreur est survenue. Veuillez réessayer.");
	if (isset($_POST['id_img']) && isset($_SESSION['id_user']))
	{
		$id_img = $_POST['id_img'];
		$id_user = $_SESSION['id_user'];
		include("../config/database.php");
		try {
			$req_img = $bdd->prepare(
				"SELECT id_like FROM ".$DB_NAME.".".$DB_TABLE_LIKES." WHERE id_user=:id_user AND id_img=:id_img"
			);
			$req_img->execute([
				':id_img' => $id_img,
				':id_user' => $id_user
			]);
		}
		catch (Exception $e) {
			$response['message'] = "Un problème est survenu.";
			exit (json_encode($response));
		}

		if ($req_img->rowCount() === 0) {
			try {
				$req_img = $bdd->prepare("INSERT INTO ".$DB_NAME.".".$DB_TABLE_LIKES." VALUES (NULL, :id_user, :id_img)");
				$req_img->execute([
					':id_img' => $id_img,
					':id_user' => $id_user
				]);
			}
			catch (Exception $e) {
				$response['message'] = "La photo n'a pas pu être liké.";
				exit(json_encode($response));
			}

			$response['success'] = true;
			$response['message'] = "La photo a bien été liké par l'utilisateur.";
			exit(json_encode($response));
		}
		else if ($req_img->rowCount() === 1) {
			try {
				$req_img = $bdd->prepare("DELETE FROM ".$DB_NAME.".".$DB_TABLE_LIKES." WHERE id_user=:id_user AND id_img=:id_img");
				$req_img->execute([
					':id_img' => $id_img,
					':id_user' => $id_user
				]);
			}
			catch (Exception $e) {
				$response['message'] = "La photo n'a pas pu être disliké." . $e->getMessage();
				exit(json_encode($response));
			}

			$response['success'] = true;
			$response['message'] = "La photo à bien été disliké.";
			exit(json_encode($response));
		}

		$response['message'] = "Un problème est survenu.";
		exit(json_encode($response));
	}
	else
	{
		exit(json_encode($response));
	}
?>
