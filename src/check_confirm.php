<?php
	if ($accept_confirm === true)
	{
		include("config/database.php");
		
		$login = $_GET['login'];
		$key = $_GET['key'];
		$request_confirm = $bdd->prepare("SELECT * FROM ".$DB_NAME.".".$DB_TABLE_USERS." WHERE login like :login ");
		$request_confirm->execute(array(':login' => $login));

		$user = $request_confirm->fetchAll()[0];
		$count = $request_confirm->rowCount();
		if ($count == 1 && $user['confirm_inscription'] == $key && $user['access'] == NON_REGISTERED_ACCESS)
		{
			$request_confirm->closeCursor();

			$request_confirm = $bdd->prepare("UPDATE ".$DB_NAME.".".$DB_TABLE_USERS." SET access = ".BASIC_ACCESS." WHERE login like :login ");

			$request_confirm->execute(array(
				':login' => $login));

			$_SESSION['id_user'] = $user['id_user'];
			$_SESSION['email'] = $user['email'];
			$_SESSION['login'] = $user['login'];
			$_SESSION['access'] = BASIC_ACCESS;
			$_SESSION['connect'] = true;
			$_SESSION['notification'] = boolval($user['notification']);

			echo "<p>Votre compte est maintenant activé.</p>";
		}
		else
		{
			$request_confirm->closeCursor();
			echo "<p>Votre compte n'as pas pu être activé veuillez contacter un administrateur.</p>";
		}
	}
?>