<?php
	$response = array('success' => false, 'login' => '', 'user' => false, 'owner_image' => false, 'nb_likes' => 0, 'liked_by_user' => false, 'message' => "Une erreur est survenue. Veuillez réessayer.");
	if (isset($_POST['id_img']))
	{
		$id_img = $_POST['id_img'];
		session_start();
		include("../config/database.php");
		try
		{
			$req_img = $bdd->prepare(
				"SELECT ".$DB_TABLE_IMAGES.".*, ".$DB_TABLE_USERS.".login FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." 
				INNER JOIN ".$DB_NAME.".".$DB_TABLE_USERS." 
				ON ".$DB_TABLE_IMAGES.".id_user = ".$DB_TABLE_USERS.".id_user 
				WHERE id_img = :id_img");
			$req_img->execute(array(':id_img' => $id_img));
		}
		catch (Exception $e)
		{
			$response['message'] = $e->getMessage();
			exit(json_encode($response));
		}

		try
		{
			$req_likes = $bdd->prepare(
				"SELECT * 
				FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." 
				INNER JOIN ".$DB_NAME.".".$DB_TABLE_LIKES." 
				ON ".$DB_TABLE_IMAGES.".id_img = ".$DB_TABLE_LIKES.".id_img 
				WHERE ".$DB_TABLE_IMAGES.".id_img = :id_img");
			$req_likes->execute(array(':id_img' => $id_img));
		}
		catch (Exception $e)
		{
			$response['message'] = $e->getMessage();
			exit(json_encode($response));
		}

		$response['nb_likes'] = $req_likes->rowCount();
		while ($like = $req_likes->fetch()) {
			if ($like['id_user'] === $_SESSION['id_user']) {
				$response['liked_by_user'] = true;
				break ;
			}
		}

		$image = $req_img->fetch();
		$count = $req_img->rowCount();
		if ($count == 1 && $image['id_img'] == $id_img && $_SESSION['id_user'] == $image['id_user']) {
			$response['user'] = true;
			$response['owner_image'] = true;
		}
		else if (isset($_SESSION['id_user'])) {
			$response['user'] = true;
		}
		
		$response['login'] = $image['login'];
		$response['success'] = true;
		$response['message'] = "Ressource récupérée.";
		$req_img->closeCursor();
		exit(json_encode($response));
	}
	else
	{
		exit(json_encode($response));
	}
?>
