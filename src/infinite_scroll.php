<?php
	$response = array('success' => false, 'nb_image' => 0, array('id_img' => "", 'path_img' => "", 'name_img' => ""));
	if (isset($_POST['offset']))
	{
		$offset = intval($_POST['offset']);
		$response['nb_image'] = $offset;
		if ($offset < 0)
		{
			exit(json_encode($response));
		}
		else
		{
			session_start();
			include("../config/database.php");

			if (isset($_POST['index']) && $_POST['index'] === "1") {
				try
				{
					$req = $bdd->prepare("SELECT * FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." 
						WHERE id_user = ".$_SESSION['id_user']." 
						ORDER BY id_img DESC 
						LIMIT 15 OFFSET :offset");
					$req->bindParam(':offset', $offset, PDO::PARAM_INT);
					$req->execute();
				}
				catch (Exception $e)
				{
					exit(json_encode($response));
				}
			}
			else {
				try
				{
					$req = $bdd->prepare("SELECT * FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." ORDER BY id_img DESC LIMIT 15 OFFSET :offset");
					$req->bindParam(':offset', $offset, PDO::PARAM_INT);
					$req->execute();
				}
				catch (Exception $e)
				{
					exit(json_encode($response));
				}
			}

			$all_images = $req->fetchAll();
			$count = $req->rowCount();

			for ($i = 0; $i < $count; $i++)
			{
				$response[$i]['id_img'] = $all_images[$i]['id_img'];
				$response[$i]['path_img'] = $all_images[$i]['path_img'];
				$response[$i]['name_img'] = $all_images[$i]['name_img'];
			}
			$response['success'] = true;
			$response['nb_image'] = $count;
			$req->closeCursor();
			exit (json_encode($response));
		}
	}
	else
	{
		exit(json_encode($response));
	}
?>