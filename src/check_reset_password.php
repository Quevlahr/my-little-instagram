<?php
	session_start();
	$response = array('success' => false, 'message' => "Une erreur est survenue. Veuillez réessayer.");
	if (isset($_POST['password']) && isset($_POST['check_password']) && isset($_SESSION['login']) && isset($_SESSION['email']) && $_SESSION['connect'] !== true)
	{
		$login = $_SESSION['login'];
		$email = $_SESSION['email'];
		$password = $_POST['password'];
		$check_password = $_POST['check_password'];
		if ($password === "" && $check_password === "")
		{
			$response['message'] = "Veuillez indiquer votre mot de passe ainsi que sa confirmation.";
			exit (json_encode($response));
		}
		else if ($password === "")
		{
			$response['message'] = "Vous n'avez pas indiqué votre mot de passe.";
			exit (json_encode($response));
		}
		else if ($check_password === "")
		{
			$response['message'] = "Vous n'avez pas confirmé votre mot de passe.";
			exit (json_encode($response));
		}
		else if ($password !== $check_password)
		{
			$response['message'] = "La confirmation du mot de passe ne correspond pas.";
			exit (json_encode($response));
		}
		else
		{
			include("../config/database.php");
			include("encrypt_password.php");

			try
			{
				$request_reset = $bdd->prepare("SELECT * FROM ".$DB_NAME.".".$DB_TABLE_USERS." WHERE login like :login ");
				$request_reset->execute(array(
					':login' => $login));
			}
			catch (Exception $e)
			{
				$response['message'] = $e->getMessage();
				exit (json_encode($response));
			}
			
			$user = $request_reset->fetch();
			$count = $request_reset->rowCount();
			
			if ($count == 1 && $user['login'] === $login)
			{
				$request_reset->closeCursor();
				
				$crypt_password = encrypt_password($password, $user['salt']);
				$reset_password = NULL;

				try
				{
					$request_reset = $bdd->prepare("UPDATE ".$DB_NAME.".".$DB_TABLE_USERS." SET reset_password = :reset_password, password = :password WHERE email = :email");
					$request_reset->execute(array(
						':reset_password' => $reset_password,
						'password' => $crypt_password,
						':email' => $email));

				}
				catch (Exception $e)
				{
					$response['message'] = $e->getMessage();
					exit (json_encode($response));
				}

				$_SESSION['id_user'] = $user['id_user'];
				$_SESSION['login'] = $user['login'];
				$_SESSION['email'] = $user['email'];
				$_SESSION['access'] = intVal($user['access']);
				$_SESSION['connect'] = true;

				$response['success'] = true;
				$response['message'] = "Le changement du mot de passe a bien été effectué.";
				exit (json_encode($response));
			}
			else
			{
				$request_reset->closeCursor();
				exit (json_encode($response));
			}
		}
	}
	else {
		exit(json_encode($response));
	}
	
?>