<?php
	function validate_email ($email)
	{
		if (filter_var($email, FILTER_VALIDATE_EMAIL))
			return (true);
		return (false);
	}

	function validate_password ($password)
	{
		if (strlen($password) >= 6)
			return (true);
		return (false);
	}

	function validate_login ($login)
	{
		if (strlen($login) >= 4)
			return (true);
		return (false);
	}

	$response = array('success' => false, 'message' => "Une erreur est survenue. Veuillez réessayer.");
	
	$id_user = (int) $_POST['id_user'];
	if (!isset($_POST['id_user']) || !ctype_digit($_POST['id_user']) || $id_user < 0) {
		exit(json_encode($response));
	}

	session_start();
	if ($_SESSION['id_user'] !== $id_user) {
		exit(json_encode($response));
	}

	if (isset($_POST['login'])) {
		$login = htmlspecialchars($_POST['login']);
		if ($login === "") {
			$response['message'] = "Veuillez indiquer un nouveau login.";
			exit(json_encode($response));
		}

		include('../config/database.php');

		if ($login === $_SESSION['login']) {
			$response['message'] = "C'est déjà votre login actuel.";
			exit(json_encode($response));
		}

		if (validate_login($login) !== true) {
			$response['message'] = "Votre login doit faire au moins 4 caractères.";
			exit(json_encode($response));
		}

		try {
			$req_user = $bdd->prepare(
				"UPDATE ".$DB_NAME.".".$DB_TABLE_USERS." SET login=:login WHERE id_user=:id_user"
			);
			$req_user->execute([
				':login' => $login,
				':id_user' => $id_user,
			]);
		}
		catch (Exception $e) {
			$regex_sql_error = "/^SQLSTATE[23000]*/";
			$regex_sql_error_uni_login = "/.*".$login.".*/";
			if (preg_match($regex_sql_error, $e->getMessage()) === 1 && 
				preg_match($regex_sql_error_uni_login, $e->getMessage()) === 1)
			{
				$response['message'] = "Ce login est déjà utilisé.";
				exit(json_encode($response));
			}
			exit(json_encode($response));
		}

		$_SESSION['login'] = $login;
		$response['success'] = true;
		$response['message'] = "Le login à bien été modifié.";
		exit(json_encode($response));
	}

	if (isset($_POST['email'])) {
		$email = htmlspecialchars($_POST['email']);
		if ($email === "") {
			$response['message'] = "Veuillez indiquer une adresse mail.";
			exit(json_encode($response));
		}

		if (validate_email($email) === false) {
			$response['message'] = "Votre adresse email n'es pas correctement formaté.";
			exit(json_encode($response));
		}

		session_start();
		include('../config/database.php');

		if ($email === $_SESSION['email']) {
			$response['message'] = "C'est déjà votre adresse mail.";
			exit(json_encode($response));
		}

		try {
			$req_user = $bdd->prepare(
				"UPDATE ".$DB_NAME.".".$DB_TABLE_USERS." SET email=:email WHERE id_user=:id_user"
			);
			$req_user->execute([
				':email' => $email,
				':id_user' => $id_user,
			]);
		}
		catch (Exception $e) {
			$regex_sql_error = "/^SQLSTATE[23000]*/";
			$regex_sql_error_uni_email = "/.*".$email.".*/";
			if (preg_match($regex_sql_error, $e->getMessage()) === 1 && 
				preg_match($regex_sql_error_uni_email, $e->getMessage()) === 1)
			{
				$response['message'] = "Cette adresse mail est déjà utilisé.";
				exit(json_encode($response));
			}
			$response['message'] = $e->getMessage();
			exit(json_encode($response));
		}

		$_SESSION['email'] = $email;
		$response['success'] = true;
		$response['message'] = "L'adresse mail à bien été modifié.";
		exit(json_encode($response));
	}

	if (isset($_POST['old_password']) && isset($_POST['new_password']) && isset($_POST['check_password'])) {
		$old_password = $_POST['old_password'];
		$new_password = $_POST['new_password'];
		$check_password = $_POST['check_password'];

		if (!validate_password($new_password) || !validate_password($old_password) || !validate_password($check_password)) {
			$response['message'] = "Votre mot de passe doit faire au moins 6 caractères.";
			exit(json_encode($response));
		}

		if ($new_password !== $check_password) {
			$response['message'] = "Les mots de passes ne correspondent pas.";
			exit(json_encode($response));
		}

		session_start();
		include("../config/database.php");
		include("encrypt_password.php");

		try {
			$req_sql = $bdd->prepare("SELECT * FROM ".$DB_NAME.".".$DB_TABLE_USERS." WHERE id_user=:id_user");
			$req_sql->execute(array(
				':id_user' => $id_user,
			));
		}
		catch (Exception $e) {
			exit (json_encode($response));
		}

		if ($req_sql->rowCount() != 1) {
			exit(json_encode($response));
		}

		$user = $req_sql->fetch();
		$crypted_password = encrypt_password($old_password, $user['salt']);
		if ($crypted_password !== $user['password']) {
			$response['message'] = "Votre mot de passe n'es pas correct.";
			exit(json_encode($response));
		}

		$new_crypted_password = encrypt_password($new_password, $user['salt']);
		try {
			$req_sql = $bdd->prepare("UPDATE ".$DB_NAME.".".$DB_TABLE_USERS." SET password=:new_password WHERE id_user=:id_user");
			$req_sql->execute([
				':new_password' => $new_crypted_password,
				':id_user' => $id_user,
			]);
		}
		catch (Exception $e) {
			$response['message'] = $e->getMessage();
			exit(json_encode($response));
		}

		$response['success'] = true;
		$response['message'] = "Le mot de passe a bien été changé.";
		exit(json_encode($response));
	}

	if (isset($_POST['notification'])) {
		$notification = htmlspecialchars($_POST['notification']);
		if ($notification === "true") {
			$notification = true;
		}
		else if ($notification === "false") {
			$notification = false;
		}
		else {
			exit(json_encode($response));
		}

		session_start();
		include('../config/database.php');

		if ($notification === $_SESSION['notification']) {
			if ($notification === false) {
				$response['message'] = "Vous n'êtes déjà pas notfié par les nouveaux commentaires.";
			}
			else {
				$response['message'] = "Vous êtes déjà notifié des nouveaux commentaires.";
			}
			exit(json_encode($response));
		}

		try {
			$req_user = $bdd->prepare(
				"UPDATE ".$DB_NAME.".".$DB_TABLE_USERS." SET notification=:notification WHERE id_user=:id_user"
			);

			$notification_db = ($notification == true) ? 1 : 0;
			$req_user->execute([
				':notification' => $notification_db,
				':id_user' => $id_user,
			]);
		}
		catch (Exception $e) {
			$response['message'] = $e->getMessage();
			exit(json_encode($response));
		}

		$_SESSION['notification'] = $notification;
		$response['success'] = true;
		if ($notification === true) {
			$response['message'] = "Vous serez notifié des nouveaux commentaires sur vos photos.";
		}
		else {
			$response['message'] = "Vous ne serez plus notifié des nouveaux commentaires sur vos photos.";
		}
		exit(json_encode($response));
	}

