<?php
	session_start();
	$response = array('success' => false, 'message' => "Une erreur est survenue. Veuillez réessayer.");
	if (isset($_POST['id_img']) && isset($_SESSION['id_user']))
	{
		$id_img = $_POST['id_img'];

		include("../config/database.php");
		try
		{
			$req_sql = $bdd->prepare("SELECT * FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." WHERE id_img = :id_img;");
			$req_sql->execute(array(
				':id_img' => $id_img));
		}
		catch (Exception $e)
		{
			$response['message'] = $e->getMessage();
			exit (json_encode($response));
		}

		$image = $req_sql->fetchAll()[0];
		$count = $req_sql->rowCount();
		if ($count != 1)
		{
			$response['message'] = "Cette photo n'existe pas ou plus.".$id_img;
			exit(json_encode($response));
		}
		if ($_SESSION['id_user'] != $image['id_user'])
		{
			$response['message'] = "Un utilisateur tente de supprimer une photo qu'il n'a prise.";
			exit(json_encode($response));
		}

		$path = "../images/creation/";
		$dossier = opendir($path);

		while (($fichier = readdir($dossier)) !== false)
		{
			if ($image['name_img'] == $fichier)
			{
				unlink($path.$fichier);
				break;
			}
		}

		closedir($dossier);

		try
		{
			$req_sql = $bdd->prepare("DELETE FROM ".$DB_NAME.".".$DB_TABLE_LIKES." WHERE id_img = :id_img");
			$req_sql->execute(array(
				':id_img' => $id_img));
		}
		catch (Exception $e)
		{
			$response['message'] = $e->getMessage();
			exit (json_encode($response));
		}


		try
		{
			$req_sql = $bdd->prepare("DELETE FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." WHERE id_img = :id_img");
			$req_sql->execute(array(
				':id_img' => $id_img));
		}
		catch (Exception $e)
		{
			$response['message'] = $e->getMessage();
			exit (json_encode($response));
		}
		

		$response['message'] = "Suppression réussie !";
		$response['success'] = true;
		exit(json_encode($response));
	}
	else {
		exit(json_encode($response));
	}
?>