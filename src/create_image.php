<?php
	$response = array('success' => false, 'message' => "Une erreur est survenue. Veuillez réessayer.", 'id_img' => false, 'name_img' => false, 'path_img' => false);

	if (!isset($_POST['frame']) || !in_array($_POST['frame'], ['frame', 'cat', 'dog', 'thing', 'tortoise', 'dontcare'])) {
		exit(json_encode($response));
	}
	$frame_name = htmlspecialchars($_POST['frame']);

	if (isset($_FILES) && isset($_FILES['file']) && ($_FILES['file']['type'] === 'image/png' || 
		$_FILES['file']['type'] === 'image/jpeg')) {

		if ($_FILES['file']['type'] === 'image/png') {
			$image = imagecreatefrompng($_FILES['file']['tmp_name']);
		}
		else if ($_FILES['file']['type'] === 'image/jpeg') {
			$image = imagecreatefromjpeg($_FILES['file']['tmp_name']);
		}

		if (!$image) {
			$response['message'] = "L'upload n'a pas pu être récupéré.";
			exit(json_encode($response));
		}
	}
	else {
		if (!isset($_POST['data'])) {
			exit(json_encode($response));
		}
		$data = $_POST['data'];

		$data = str_replace('data:image/png;base64,', '', $data);
		$data = str_replace(' ', '+', $data);

		$data = base64_decode($data);
		if ($data === false) {
			exit(json_encode($response));
		}
		
		$image = imagecreatefromstring($data);
		if ($image === false) {
			exit(json_encode($response));
		}
	}

	session_start();
	include("../config/database.php");
	header("Content-Type: image/png");

	$id_user = $_SESSION['id_user'];

	$path = "../images/creation/";
	$name_img = uniqid("photo_", true).".png";


	$frame = imagecreatefrompng("../images/frames/".$frame_name.".png");

	if ($frame_name === "cat") {
		$dst_x = 320;
		$dst_y = 190;
	}
	else if ($frame_name === "dog") {
		$dst_x = 430;
		$dst_y = 310;
	}
	else if ($frame_name === "dontcare") {
		$dst_x = 300;
		$dst_y = 0;
	}
	else {
		$dst_x = 0;
		$dst_y = 0;		
	}

	$base_image = imagecreatetruecolor(540, 405);

	imagealphablending($base_image, false);
	if (($color = imagecolorallocatealpha($base_image, 0, 0, 0, 127)) === false) {
		exit(json_encode($response));
	}
	imagefilledrectangle($base_image, 0, 0, 540, 405, $color);
	imagealphablending($base_image, true);    
	imagesavealpha($base_image, true);

	if (!imagecopyresized($base_image, $image, 0, 0, 0, 0, 540, 405, imagesx($image), imagesy($image))) {
		exit(json_encode($response));
	}
	if (!imagecopy($base_image, $frame, $dst_x, $dst_y, 0, 0, imagesx($frame), imagesy($frame))) {
		exit(json_encode($response));
	}
	if (!imagepng($base_image, $path.$name_img)) {
		exit(json_encode($response));
	}

	$size = getimagesize($path.$name_img);
	if ($size === false) {
		exit(json_encode($response));
	}

	$width = $size[0];
	$height = $size[1];
	$type = $size['mime'];
	$path = "images/creation/".$name_img;
	$last_id = 0;

	try {
		$new_img = $bdd->prepare("INSERT IGNORE INTO ".$DB_NAME.".".$DB_TABLE_IMAGES." VALUES (NULL, :id_user, :name, :width, :height, :type, :path);");
		$new_img->execute(array(
			':id_user' => $id_user,
			':name' => $name_img,
			':width' => $width,
			':height' => $height,
			':type' => $type,
			':path' => $path
		));
		$last_id = $bdd->lastInsertId();
	}
	catch (Exception $e)
	{
		$response['message'] = $e->getMessage();
		exit(json_encode($response));
	}

	imagedestroy($base_image);
	imagedestroy($image);
	imagedestroy($frame);

	$response['id_img'] = $last_id;
	$response['path_img'] = $path;
	$response['name_img'] = $name_img;
	$response['message'] = "La photo a bien été enregistrée.";
	
	$response['success'] = true;
	exit(json_encode($response));
?>