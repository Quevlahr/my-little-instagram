<?php
	function send_email_inscription($email, $login, $key)
	{
		$sender_email = "postmaster@qroulon.fr";
		$adress = "http://".$_SERVER['HTTP_HOST']."/camagru/confirm.php";
		ini_set("SMTP", "smtp.qroulon.fr");


		$subject = "Activer votre compte";

		$headers = "From: $sender_email";
		$headers = "MIME-Version: 1.0"."\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1"."\r\n";

		$adress = $adress."?login=".urlencode($login)."&key=".urlencode($key);

		$message = "<h2>Bienvenue sur Camagru,</h2>

		<p>Pour activer votre compte, veuillez cliquer sur le lien ci dessous
		ou le copier/coller dans votre navigateur internet.</p>

		<a href=\"".$adress."\">".$adress."</a>
		<br>
		<p>---------------</p>
		<p>Ceci est un mail automatique, Merci de ne pas y répondre.</p>";


		if (mail($email, $subject, $message, $headers)) {
			return true;
		}
		return false;
	}

	function send_email_reset($email, $login, $key)
	{
		$sender_email = "postmaster@qroulon.fr";
		$adress = "http://".$_SERVER['HTTP_HOST']."/camagru/reset_password.php";
		ini_set("SMTP", "smtp.qroulon.fr");

		$subject = "Reset de votre mot de passe";

		$headers = "From: $sender_email";
		$headers = "MIME-Version: 1.0"."\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1"."\r\n";

		$adress = $adress."?login=".urlencode($login)."&key=".urlencode($key);

		$message = "<h2>Bonjour ".$login.",</h2>

		<p>Pour changer votre mot de passe, veuillez cliquer sur le lien ci dessous
		ou le copier/coller dans votre navigateur internet.</p>

		<a href=\"".$adress."\">".$adress."</a>
		<br>
		<p>---------------</p>
		<p>Ceci est un mail automatique, Merci de ne pas y répondre.</p>";


		if (mail($email, $subject, $message, $headers)) {
			return true;
		}
		return false;
	}

	function send_email_notification($email, $login, $id_image, $login_comment)
	{
		$sender_email = "postmaster@qroulon.fr";
		$adress = "http://".$_SERVER['HTTP_HOST']."/camagru/gallery.php";
		ini_set("SMTP", "smtp.qroulon.fr");

		$subject = "Une de vos photo à été commentée";

		$headers = "From: $sender_email";
		$headers = "MIME-Version: 1.0"."\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1"."\r\n";

		$adress = $adress."?id_img=".urlencode($id_image);

		$message = "<h2>Bonjour ".$login.",</h2>

		<p>Votre photo à été commentée par ".$login_comment."</p>

		<a href=\"".$adress."\">".$adress."</a>
		<br>
		<p>---------------</p>
		<p>Ceci est un mail automatique, Merci de ne pas y répondre.</p>";


		if (mail($email, $subject, $message, $headers)) {
			return true;
		}
		return false;
	}
?>