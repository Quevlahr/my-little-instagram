<?php
	session_start();

	$response = array('success' => false, 'message' => "Une erreur est survenue. Veuillez réessayer.");

	if ($_SESSION['connect'] !== true || !isset($_POST['id_img']) || !isset($_POST['comment'])) {
		exit(json_encode($response));
	}

	$id_img = (int) $_POST['id_img'];
	if (!ctype_digit($_POST['id_img']) || $id_img < 0) {
		exit(json_encode($response));
	}

	$comment = htmlspecialchars($_POST['comment']);
	if ($comment === "" || is_null($comment)) {
		$response['message'] = "Veuillez entrer un commentaire.";
		exit(json_encode($response));
	}

	include('../config/database.php');
	try {
		$req_sql = $bdd->prepare("INSERT INTO ".$DB_NAME.".".$DB_TABLE_COMMENTS." VALUES (NULL, :comment, :id_user, :id_img)");
		$req_sql->execute([
			':comment' => $comment,
			':id_user' => $_SESSION['id_user'],
			':id_img' => $id_img
		]);
	}
	catch (Exception $e) {
		exit(json_encode($response));
	}

	try {
		$req_sql = $bdd->prepare("SELECT * FROM ".$DB_NAME.".".$DB_TABLE_USERS." INNER JOIN ".$DB_NAME.".".$DB_TABLE_IMAGES." ON ".$DB_TABLE_IMAGES.".id_user = ".$DB_TABLE_USERS.".id_user WHERE id_img = :id_img");
		$req_sql->execute([
			':id_img' => $id_img,
		]);
	}
	catch (Exception $e) {
		$response['message'] = $e->getMessage();
		exit(json_encode($response));
	}

	$creator = $req_sql->fetch();

	if (!$creator || $req_sql->rowCount() !== 1) {
		$response['message'] = 'ya personne';
		exit(json_encode($response));
	}

	if ($creator['id_user'] !== $_SESSION['id_user'] && $creator['notification'] == true) {
		$creator['notification'] = 'hohoo';
		include("send_email.php");
		if (send_email_notification($creator['email'], $creator['login'], $creator['id_img'], $_SESSION['login']) === false)
		{
			$response['message'] = "Le mail de notification n'a pas pu être envoyé.";
			exit(json_encode($response));
		}	
	}

	$response['success'] = true;
	$response['message'] = "Le commentaire à bien été posté.".$creator['email'];

	exit(json_encode($response));
?>