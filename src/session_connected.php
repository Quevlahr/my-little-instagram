<?php
	include("src/variables.php");
	if (!isset($_SESSION))
	{
		session_start();
	}
	if (!isset($_SESSION['connect']) || $_SESSION['connect'] === false)
	{
		header("location: error.php");
	}
?>