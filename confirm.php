<?php
	include("src/session_non_limited.php");
	$accept_confirm = false;
	if (!isset($_GET['login']) || !isset($_GET['key']) ||
		strlen($_GET['login']) < 4 || 
		strlen($_GET['login']) >= 255 ||
		$_GET['key'] == "")
	{
		header("location: error.php");
	}
	else if ($_SESSION['connect'] === true && $_SESSION['access'] != NON_REGISTERED_ACCESS)
	{
		header("location: index.php");
	}
	else
	{
		$accept_confirm = true;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("src/head_html.php"); ?>
	</head>
	<body>

		<div id="all">
			<?php include("src/header.php"); ?>
			<div id="content">
				<h1>Confirmation</h1>
				<?php
					include("src/check_confirm.php");
				?>
				<div id="marge"></div>
				<p>Accueil : <a href="index.php" title="Camagru">Camagru</a></p>
			</div>
			<?php include("src/footer.php"); ?>
		</div>

	</body>
</html>