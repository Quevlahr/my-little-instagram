<?php
	include("src/session_connected.php");
	include("config/database.php");
	try
	{
		$req_sql = $bdd->query("SELECT * 
				FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." 
				WHERE id_user = ".$_SESSION['id_user']." 
				ORDER BY id_img DESC LIMIT 6 OFFSET 0");
	}
	catch (Exception $e)
	{
		exit($e->getMessage());
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("src/head_html.php"); ?>
		<script type="text/javascript" src="js/editing.js" async ></script>
		<link rel="stylesheet"  type="text/css" href="css/formulaire.css" />
		<link rel="stylesheet"  type="text/css" href="css/editing.css" />
	</head>
	<body>

		<div id="all">
			<?php include("src/header.php"); ?>
			<div id="content">
				<h1>Montage Photo.</h1>
				<div id="container_editing">
					<div id="main_editing">
						<p class="error_msg" id="error_img">Veuillez prendre une photo.</p>

						<video id="video"></video>

						<form id="frame_editing" method="post">
							<div class="frames_inputs">
								<label>
									<input type="radio" value="frame" name="frame"></input>
									cadre
								</label>
								<label>
									<input type="radio" value="cat" name="frame"></input>
									chat
								</label>
								<label>
									<input type="radio" value="dog" name="frame"></input>
									chien
								</label>
								<label>
									<input type="radio" value="thing" name="frame"></input>
									chose
								</label>
								<label>
									<input type="radio" value="tortoise" name="frame"></input>
									tortue
								</label>
								<label>
									<input type="radio" value="dontcare" name="frame"></input>
									ballec
								</label>
							</div>
								
							<div class="button_group">
								<input id="input_file" type="file" name="file">
								<button type="submit" class="button_form" id="startbutton">Photo</button>
							</div>
						</form>
					</div>
					<div id="side_editing">

						<?php
							if ($req_sql->rowCount() != 0)
							{
								while($image = $req_sql->fetch())
								{
									echo "<div class=\"item_side_editing\"><img id=\"".$image['id_img']."\"src=\"".$image['path_img']."\" alt=\"".$image['name_img']."\"></div>";
								}
							}
							else
							{
								?>
								<p id="no_image">Il n'y a pas encore de montages.</p>
								<?php
							}
						?>
					</div>
				</div>
				<canvas id="canvas"></canvas>
				<div id="marge"></div>
			</div>
			<?php include("src/footer.php"); ?>
		</div>

	</body>
</html>