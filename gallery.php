<?php
	include("src/session_non_limited.php");
	include("config/database.php");

	$infinite = true;
	if (isset($_GET['id_img']) && is_numeric($_GET['id_img']) && $_GET['id_img'] > 0) {
		$id_img = (int)$_GET['id_img'];
		try
		{
			$req_sql = $bdd->prepare("SELECT * FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." WHERE ".$DB_TABLE_IMAGES.".id_img = :id_img");
			$req_sql->execute([
				':id_img' => $id_img,
			]);

			if ($req_sql->rowCount() <= 0) {
				header("location: error.php");
			}

			$req_likes = $bdd->prepare("SELECT * FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." INNER JOIN ".$DB_NAME.".".$DB_TABLE_LIKES." ON ".$DB_TABLE_IMAGES.".id_img = ".$DB_TABLE_LIKES.".id_img WHERE ".$DB_TABLE_IMAGES.".id_img = :id_img");
			$req_likes->execute([
				':id_img' => $id_img,
			]);

			$req_comments = $bdd->prepare("SELECT comment, login 
				FROM ".$DB_NAME.".".$DB_TABLE_COMMENTS." 
				INNER JOIN ".$DB_NAME.".".$DB_TABLE_USERS." 
				ON ".$DB_TABLE_COMMENTS.".id_user = ".$DB_TABLE_USERS.".id_user 
				WHERE id_img = :id_img ORDER BY id_comment DESC");
			$req_comments->execute([
				':id_img' => $id_img
			]);
			$infinite = false;
		}
		catch (Exception $e)
		{
			exit($e->getMessage());
		}

	}
	else {
		try
		{
			$req_sql = $bdd->query("SELECT id_img, path_img, name_img FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." ORDER BY id_img DESC LIMIT 20 OFFSET 0");
		}
		catch (Exception $e)
		{
			exit($e->getMessage());
		}
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("src/head_html.php"); ?>
		<link rel="stylesheet"  type="text/css" href="css/gallery.css" />
		<link rel="stylesheet"  type="text/css" href="css/formulaire.css" />
	</head>
	<body>

		<div id="all">
			<?php include("src/header.php"); ?>
			<div id="content">
				<?php
					if ($infinite === true) {
				?>
				<div id="modal_gallery" class="modal hidden">
					<div class="modal_content">
						<p id="img_take_by">Photo prise par : <span id="creator_img"></span></p>
						<span class="close">&times;</span>
						<img class="modal_image" src="">
						<p>
							<div class=" thumb_up">
								<span id="nb_likes">0</span>
								<i class="material_icons">thumb_up</i>
							</div>
						</p>
						<div id="modal_options">
						</div>
						<div id="comment_part"></div>
					</div>
				</div>
				<h1>Galerie</h1>

				<div id="unique_gallery" class="gallery">
				<?php
					if ($req_sql->rowCount() != 0)
					{
						while($image = $req_sql->fetch())
						{
							echo "<div class=\"item_gallery\">
									<a class=\"link_image\" href=\"gallery.php?id_img=".$image['id_img']."\" title=\"\">
										<img id=\"".$image['id_img']."\"src=\"".$image['path_img']."\" alt=\"".$image['name_img']."\">
									</a>
								</div>";
						}
					}
					else
					{
						?>
						<p>Il n'y a pas encore de montages.</p>
						<?php
					}
				?>
				</div>
				<p id="load_infinite_scroll"></p>
				<?php
					}
					else if ($infinite === false) {
						$image = $req_sql->fetch();
						echo "<div class=\"no_gallery\">
							<img id=\"".$image['id_img']."\"src=\"".$image['path_img']."\" alt=\"".$image['name_img']."\">
							</div>";
				?>
						<p>
							<div class=" thumb_up">
								<span id="nb_likes"><?php echo $req_likes->rowCount(); ?></span>
								<i class="material_icons">thumb_up</i>
							</div>
						</p>
						<?php if (isset($_SESSION) && isset($_SESSION['connect']) && $_SESSION['connect'] === true) { ?>
						<div>
							<?php
								$liked_by_user = "unliked";
								while ($like = $req_likes->fetch()) {
									if ($like['id_user'] === $_SESSION['id_user']) {
										$liked_by_user = "liked";
										break ;
									}
								}
							?>
							<button id="button_like" class=<?php echo "button_form_".$liked_by_user; ?>>J'aime</button>
							<?php if (isset($_SESSION['id_user']) && $_SESSION['id_user'] === $image['id_user']) { ?>
							<button id="button_delete" class="button_form">Supprimer</button>
							<?php } ?>
						</div>
						<?php } ?>
						<div id="comment_part">
							<div id="comment_display">
								<?php
									while ($comment = $req_comments->fetch()) {
								?>
								<div><?php echo $comment['login']; ?></div>
								<p><?php echo $comment['comment']; ?></p>
								<?php
									}
								?>
							</div>
							<?php if (isset($_SESSION) && $_SESSION['connect'] === true) { ?>
							<form id="comment_form">
								<textarea id="textarea_comment" class="modal_comment"></textarea>
								<button type="submit" class="send">
									<i class="material_icons">send</i>
								</button>
							</form>
							<?php } ?>
						</div>
				<?php
					}
				?>
				<div id="marge"></div>
			</div>
			<?php include("src/footer.php"); ?>
		</div>

    <?php if ($infinite === true) { ?>
        <script type="text/javascript" src="js/gallery.js"></script>
        <script type="text/javascript"> var index = 0; </script>
    <?php } else if ($infinite === false) { ?>
        <script type="text/javascript" src="js/no_gallery.js"></script>
    <?php } ?>
	</body>
</html>