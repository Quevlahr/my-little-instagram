<?php
	include("src/session_non_limited.php");
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("src/head_html.php"); ?>
	</head>
	<body>

		<div id="all">
			<?php include("src/header.php"); ?>
			<div id="content">
				<h1>Erreur</h1>
				<p>Veuillez réessayer dans quelques instants. Sinon il est possible que la page a laquelle vous tentez d'accéder n'existe pas, ou est inaccessible car vous n'êtes pas connecté.</p>
			</div>
			<?php include("src/footer.php"); ?>
		</div>

	</body>
</html>