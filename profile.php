<?php
	include("src/session_connected.php");
	include("config/database.php");
	try
	{
		if (isset($_SESSION['connect']) && $_SESSION['connect'] === true &&
			isset($_SESSION['access']) && $_SESSION['access'] !== NON_REGISTERED_ACCESS)
		{
			$req_sql = $bdd->query(
				"SELECT * 
				FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." 
				WHERE id_user = ".$_SESSION['id_user']." 
				ORDER BY id_img DESC LIMIT 12 OFFSET 0");
		}
		else
		{
			$req_sql = $bdd->query("SELECT id_img, path_img, name_img FROM ".$DB_NAME.".".$DB_TABLE_IMAGES." ORDER BY id_img DESC LIMIT 12 OFFSET 0");
		}
	}
	catch (Exception $e)
	{
		exit($e->getMessage());
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("src/head_html.php"); ?>
		<link rel="stylesheet"  type="text/css" href="css/formulaire.css" />
	</head>
	<body>
		<div id="all">
			<?php include("src/header.php"); ?>
			<div id="content">
				<?php
					if (isset($_SESSION['connect']) && $_SESSION['connect'] === true &&
						isset($_SESSION['access']) && $_SESSION['access'] !== NON_REGISTERED_ACCESS)
					{
				?>
					<div class="card">
						<div class="card_title">
							<h1>Modification de votre profil</h1>
						</div>
						<div class="container">
							<div>
								<p class="error_msg" id="error_msg_id">Veuillez indiquer vos données</p>
							</div>

							<form id="change_login" class="form_connexion" method="post">
								<fieldset>
									<div class="input_group">
										<input id="change_login_input" class="input_form" name="login" type="text" required>
										<span class="input_bar"></span>
										<label id="change_login_label" for="login"><?php echo $_SESSION['login']; ?></label>
									</div>
									<div class="button_group">
										<button id="submit_change_login" class="button_form" name="save_change_login" type="submit">Sauvegarder</button>
									</div>
								</fieldset>
							</form>

							<form id="change_email" class="form_connexion" method="post">
								<fieldset>
									<div class="input_group">
										<input id="change_email_input" class="input_form" name="email" type="text" required>
										<span class="input_bar"></span>
										<label id="change_email_label" for="email"><?php echo $_SESSION['email']; ?></label>
									</div>
									<div class="button_group">
										<button id="submit_change_email" class="button_form" name="save_change_email" type="submit">Sauvegarder</button>
									</div>
								</fieldset>
							</form>

							<form id="change_password" class="form_connexion" method="post">
								<fieldset>
									<div class="input_group">
										<input id="change_old_password_input" class="input_form" name="old_password" type="password" required>
										<span class="input_bar"></span>
										<label for="old_password">Ancien mot de passe</label>
									</div>
									<div class="input_group">
										<input id="change_new_password_input" class="input_form" name="new_password" type="password" required>
										<span class="input_bar"></span>
										<label for="new_password">Nouveau mot de passe</label>
									</div>
									<div class="input_group">
										<input id="change_check_password_input" class="input_form" name="check_password" type="password" required>
										<span class="input_bar"></span>
										<label for="check_password">Confirmation mot de passe</label>
									</div>
									<div class="button_group">
										<button id="submit_form_new_password" class="button_form" name="send" type="submit">Sauvegarder</button>
									</div>
								</fieldset>
							</form>

							<form id="change_notification" class="form_connexion" method="post">
								<fieldset>
									<div class="input_checkbox">
										<label for="notification">Notifications par mail</label>
										<input id="change_notification_input" class="" name="notification" type="checkbox" <?php if ($_SESSION['notification']) {echo "checked";} ?>>
									</div>
									<div class="button_group">
										<button id="submit_change_notification" class="button_form" name="save_change_notification" type="submit">Sauvegarder</button>
									</div>
								</fieldset>
							</form>

						</div>
					</div>
				<?php
					}
				?>

				<div id="marge"></div>
			</div>
			<?php include("src/footer.php"); ?>
		</div>

		<?php
			if (isset($_SESSION['connect']) && $_SESSION['connect'] === true &&
				isset($_SESSION['access']) && $_SESSION['access'] !== NON_REGISTERED_ACCESS) {
		?>
		<script type="text/javascript">
			var id_user = <?php echo ($_SESSION['id_user']); ?>;
		</script>
		<script type="text/javascript" src="js/profile.js" async ></script>
		<?php } ?>
	</body>
</html>