<?php
	include("src/session_non_connected.php");
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("src/head_html.php"); ?>
		<link rel="stylesheet"  type="text/css" href="css/formulaire.css" />
	</head>
	<body>

		<div id="all">
			<?php include("src/header.php"); ?>
			
			<div id="content">
				<div class="card" id="login">
					<div class="card_title">
						<h1>Connexion</h1>
					</div>
					<div class="container">
						<div>
							<p class="error_msg" id="error_connexion">Veuillez indiquer vos données</p>
						</div>
						<form id="connexion" class="form_connexion" method="post">
							<fieldset>
								<div class="input_group">
									<input class="input_form" name="login" type="text" required>
									<span class="input_bar"></span>
									<label for="login">Login</label>
								</div>
								<div class="input_group">
									<input class="input_form" name="password" type="password" required>
									<span class="input_bar"></span>
									<label for="password">Mot de passe</label>
								</div>
								<div class="button_group">
									<button id="submit_form_connexion" class="button_form" name="connection" type="submit">Connexion</button>
								</div>
							</fieldset>
						</form>
					</div>
					<div class="simple_link">
						<a class="change_card" href="forget_password.php">Mot de passe oublié</a>
					</div>
				</div>

				<div class="card hidden" id="give_email">
					<div class="card_title">
						<h1>Mot de passe oublié</h1>
					</div>
					<div class="container">
						<div>
							<p class="error_msg" id="error_give_email">Veuillez indiquer l'adresse mail avec laquelle vous vous êtes inscrit.</p>
						</div>
						<form id="form_give_email" class="form_connexion" method="post">
							<fieldset>
								<div class="input_group">
									<input class="input_form" name="email" type="text" required>
									<span class="input_bar"></span>
									<label for="email">Email</label>
								</div>
								<div class="button_group">
									<button id="submit_form_email" class="button_form" name="send" type="submit">Envoyer</button>
								</div>
							</fieldset>
						</form>
					</div>
					<div class="simple_link">
						<a class="change_card" href="login.php">Se connecter</a>
					</div>
				</div>

				<div id="marge"></div>
			</div>

			<?php include("src/footer.php"); ?>
		</div>

		<script type="text/javascript" src="js/login.js"></script>
		<script type="text/javascript" src="js/forget_password.js"></script>

	</body>
</html>